<pre>
6.1.a. Analyzing network interfaces

    1. Begin by examining the network settings on your computer. Next, perform a thorough check on all available network interfaces on the system using the command "ifconfig /ip".

ubuntu@ip-172-31-42-130:~$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:41:52:ce brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.13/24 metric 100 brd 10.0.2.255 scope global dynamic enp0s3
       valid_lft 335sec preferred_lft 335sec
    inet6 fe80::a00:27ff:fe41:52ce/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:17:14:e6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.77/24 metric 100 brd 192.168.0.255 scope global dynamic enp0s8
       valid_lft 5750sec preferred_lft 5750sec
    inet6 fe80::a00:27ff:fe17:14e6/64 scope link
       valid_lft forever preferred_lft forever
	  
The "lo" interface is UP and has been assigned an IP address of "127.0.0.1" with a subnet mask of "/8". The "valid_lft" and "preferred_lft" values indicate that this IP address will remain valid and preferred indefinitely.

The interface also has an IPv6 address assigned to it, which is "::1/128". The "::1" address is the IPv6 equivalent of the IPv4 loopback address "127.0.0.1".

For "enp0s3", the output shows that it is currently UP and has been assigned an IP address of "10.0.2.13" with a subnet mask of "/24". The "valid_lft" and "preferred_lft" values indicate the length of time that this IP address will remain valid and preferred, respectively. The interface also has an IPv6 address assigned to it.

For "enp0s8", the output shows that it is also UP and has been assigned an IP address of "192.168.0.77" with a subnet mask of "/24".	  

The "mtu" value in the output of the "ip a" command refers to the Maximum Transmission Unit (MTU) of the network interface. The MTU is the largest size of data packet that can be transmitted over the network interface without fragmentation.

In the output you provided, both "eth0" and "eth1" have an MTU value of "1500". This is the default MTU value for Ethernet networks and is the most common MTU value used in modern networks.

The "lo" interface has an MTU value of "65536", which is much larger than the MTU value for Ethernet networks. This is because the loopback interface is a virtual interface that does not have the same physical limitations as a physical network interface.


    2. Check the quality of the connection to domains such as ukr.net, google.com, 8.8.8.8, and others. Use the ping command to test the connection and analyze the output.

The ping command sends packets of data to a specified network address and measures the time it takes for the packets to be sent and received. The output of the ping command includes information about the number of packets sent and received, the round-trip time for each packet, and any errors or timeouts that occur during the test.

By analyzing the output of the ping command, you can determine the quality of the connection to the specified network address. A successful ping test indicates that the network connection is working properly, while a failed ping test may indicate a problem with the network or the target address.

ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -c 4 -i 0.2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) from 10.0.2.13 enp0s3: 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=117 time=18.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=117 time=17.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=117 time=18.3 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=117 time=23.7 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 602ms
rtt min/avg/max/mdev = 17.395/19.416/23.670/2.483 ms

Metric Name: Network (8.8.8.8)
Unit: B
Maximum: 1636
Data Series: Receive Rate
0 0 0 0 0 0 0 0 256 0 0 0 192 158 554 588 0 64 0 0 374 0 192 96 0 256 0 0 0 0 0 0 0 
Data Series: Transmit Rate
0 0 0 0 0 0 0 0 60 0 0 0 240 150 1026 1636 0 60 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0  

ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -c 4 -i 0.2 ukr.net
PING ukr.net (104.18.9.128) from 10.0.2.13 enp0s3: 56(84) bytes of data.
64 bytes from 104.18.9.128 (104.18.9.128): icmp_seq=1 ttl=57 time=4.33 ms
64 bytes from 104.18.9.128 (104.18.9.128): icmp_seq=2 ttl=57 time=3.35 ms
64 bytes from 104.18.9.128 (104.18.9.128): icmp_seq=3 ttl=57 time=3.22 ms
64 bytes from 104.18.9.128 (104.18.9.128): icmp_seq=4 ttl=57 time=3.04 ms

--- ukr.net ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 604ms
rtt min/avg/max/mdev = 3.044/3.487/4.331/0.498 ms

Metric Name: Network (ukr.net)
Unit: B
Maximum: 1950
Data Series: Receive Rate
0 0 0 0 0 0 0 0 256 0 0 0 192 158 554 588 0 64 0 0 374 0 192 96 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 247 0 256 0 0 0 286 0 0 0 0 0 722 0 0 166 158 1076 0 166 0 256 1948 1950 0 0 0 0 0 96 96 96 0 0 0 256 0 0 0 0 
Data Series: Transmit Rate
0 0 0 0 0 0 0 0 60 0 0 0 240 150 1026 1636 0 60 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 0 70 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 330 0 0 0 0 342 120 0 0 122 98 648 0 98 0 60 1842 1752 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0

ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -c 4 -i 0.2 google.com
PING google.com (216.58.215.78) from 10.0.2.13 enp0s3: 56(84) bytes of data.
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=1 ttl=117 time=16.9 ms
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=2 ttl=117 time=16.9 ms
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=3 ttl=117 time=18.0 ms
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=4 ttl=117 time=19.6 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 621ms
rtt min/avg/max/mdev = 16.917/17.859/19.604/1.099 ms

Metric Name: Network (google.com)
Unit: B
Maximum: 2176
Data Series: Receive Rate
0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 247 0 256 0 0 0 286 0 0 0 0 0 722 0 0 166 158 1076 0 166 0 256 1948 1950 0 0 0 0 0 96 96 96 0 0 0 256 0 0 0 0 0 374 0 0 0 0 0 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 0 0 566 0 0 0 0 64 166 0 0 0 0 64 474 632 256 260 0 0 0 1708 562 0 0 0 64 0 192 96 256 0 0 0 
Data Series: Transmit Rate
0 70 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 330 0 0 0 0 342 120 0 0 122 98 648 0 98 0 60 1842 1752 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 240 0 0 0 0 60 122 0 0 0 0 60 294 392 60 248 0 0 0 2176 1377 0 0 0 60 0 0 0 60 0 0 0 

ubuntu@ip-172-31-42-130:~$ ping -I enp0s8 -c 4 -i 0.2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) from 192.168.0.77 enp0s8: 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=20.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=17.3 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=118 time=16.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=118 time=17.5 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 603ms
rtt min/avg/max/mdev = 16.196/17.974/20.875/1.747 ms

Metric Name: Network (8.8.8.8)
Unit: B
Maximum: 2518
Data Series: Receive Rate
0 0 0 0 374 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 374 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 158 0 0 256 0 64 0 192 0 0 0 0 0 374 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 0 102 64 0 166 706 2104 166 0 158 0 1078 0 0 0 0 320 0 0 0 0 0 96 96 96 0 0 0 0 0 256 
Data Series: Transmit Rate
0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 90 0 0 60 0 60 0 240 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 130 0 0 114 196 1800 98 0 98 0 2518 0 0 0 0 120 0 0 0 0 0 0 0 0 0 0 0 0 0 60 

ubuntu@ip-172-31-42-130:~$ ping -I enp0s8 -c 4 -i 0.2 ukr.net
PING ukr.net (104.18.8.128) from 192.168.0.77 enp0s8: 56(84) bytes of data.
64 bytes from 104.18.8.128 (104.18.8.128): icmp_seq=1 ttl=58 time=4.01 ms
64 bytes from 104.18.8.128 (104.18.8.128): icmp_seq=2 ttl=58 time=3.06 ms
64 bytes from 104.18.8.128 (104.18.8.128): icmp_seq=3 ttl=58 time=2.95 ms
64 bytes from 104.18.8.128 (104.18.8.128): icmp_seq=4 ttl=58 time=2.53 ms

--- ukr.net ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 604ms
rtt min/avg/max/mdev = 2.527/3.134/4.010/0.542 ms

Metric Name: Network (ukr.net)
Unit: B
Maximum: 4154
Data Series: Receive Rate
374 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 0 102 64 0 166 706 2104 166 0 158 0 1078 0 0 0 0 320 0 0 0 0 0 96 96 96 0 0 0 0 0 256 0 0 0 192 374 0 0 0 64 0 0 0 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 374 0 0 0 0 0 0 166 0 166 166 1084 1314 166 158 0 4154 0 0 0 0 128 64 0 96 192 0 0 0 0 630 
Data Series: Transmit Rate
0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 130 0 0 114 196 1800 98 0 98 0 2518 0 0 0 0 120 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 240 0 0 0 0 60 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 122 0 138 114 900 990 98 98 0 3662 0 0 0 0 120 0 0 0 0 0 0 0 0 60

ubuntu@ip-172-31-42-130:~$ ping -I enp0s8 -c 4 -i 0.2 google.com
PING google.com (216.58.215.78) from 192.168.0.77 enp0s8: 56(84) bytes of data.
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=1 ttl=118 time=18.4 ms
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=2 ttl=118 time=19.5 ms
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=3 ttl=118 time=25.4 ms
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=4 ttl=118 time=16.0 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 604ms
rtt min/avg/max/mdev = 16.005/19.832/25.377/3.445 ms

Metric Name: Network (google.com)
Unit: B
Maximum: 4154
Data Series: Receive Rate
0 192 374 0 0 0 64 0 0 0 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 374 0 0 0 0 0 0 166 0 166 166 1084 1314 166 158 0 4154 0 0 0 0 128 64 0 96 192 0 0 0 0 630 0 0 0 192 0 0 0 0 0 0 0 0 0 0 0 0 0 0 256 0 0 0 0 0 0 0 0 102 230 332 1938 664 422 0 0 0 158 0 0 2130 0 0 0 0 128 0 0 96 192 0 128 128 0 0 0 0 0 
Data Series: Transmit Rate
0 240 0 0 0 0 60 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 122 0 138 114 900 990 98 98 0 3662 0 0 0 0 120 0 0 0 0 0 0 0 0 60 0 0 0 240 0 0 0 0 0 0 0 0 0 0 0 0 0 0 60 0 0 0 0 0 0 0 0 122 106 228 1710 360 158 0 0 0 98 0 0 3510 0 0 0 0 120 0 0 0 0 0 60 0 0 0 0 0 0 

The output for the enp0s8 ping 8.8.8.8 command shows that all 4 packets were successfully transmitted and received, with an average round-trip time of 17.974 ms. This is slightly faster than the ping command from enp0s3, which had an average round-trip time of 19.416 ms.

The network metrics for the IP address 8.8.8.8 are also different between the two interfaces. The maximum amount of data received or transmitted in a single second is higher for enp0s8 (2518 bytes) compared to enp0s3 (1636 bytes). This suggests that the network connection through the enp0s8 interface may be faster or more reliable than the enp0s3 interface.

    3. To assess the quality of a host's connection and stress test its network interface, use commands like ping, mtr, and traceroute.

ubuntu@ip-172-31-42-130:~$ mtr -I enp0s3 google.com
                                  My traceroute  [v0.95]
                                   My traceroute  [v0.95]
ip-172-31-42-130 (10.0.2.13) -> google.com (216.58.215.78)       2023-08-09T13:48:50+0000
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                                 Packets               Pings
 Host                                          Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. _gateway                                    0.0%    10    0.5   0.5   0.3   0.9   0.2
 2. _gateway                                    0.0%     9    2.6   4.5   2.6   9.2   2.2
 3. 254.20.150.178.triolan.net                  0.0%     9    4.2   6.1   3.0  11.4   2.7
 4. 10.131.109.109                              0.0%     9    6.1   5.9   3.1  13.0   3.1
 5. 226.3.86.109.triolan.net                    0.0%     9    6.9  40.0   3.8 309.4 101.1
 6. 108.170.248.138                             0.0%     9    3.6  47.5   3.4 216.8  83.8
 7. 72.14.239.111                              11.1%     9    5.1  20.0   3.9 124.4  42.2
 8. 142.251.242.41                              0.0%     9   18.5  55.7  18.1 317.1  98.1
 9. 108.170.250.209                             0.0%     9   18.8  41.9  18.1 224.6  68.5
10. 108.170.234.103                             0.0%     9   19.0  60.1  18.0 277.7  89.8
11. waw02s16-in-f14.1e100.net                  11.1%     9   17.3  41.1  16.9 177.4  55.5

ubuntu@ip-172-31-42-130:~$ mtr -I enp0s8 google.com
                                   My traceroute  [v0.95]
ip-172-31-42-130 (192.168.0.77) -> google.com (216.58.215.78)    2023-08-09T13:50:33+0000
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                                 Packets               Pings
 Host                                          Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. _gateway                                   20.0%     6    2.0   2.8   1.6   5.7   1.9
 2. 254.20.150.178.triolan.net                 80.0%     6    3.9   3.9   3.9   3.9   0.0
 3. 10.131.109.109                             20.0%     6    3.9   4.1   2.4   7.2   2.1
 4. 226.3.86.109.triolan.net                   80.0%     6  305.9 305.9 305.9 305.9   0.0
 5. 108.170.248.138                            20.0%     6    3.0   9.6   3.0  24.2   9.9
 6. 72.14.239.111                              80.0%     6  123.0 123.0 123.0 123.0   0.0
 7. 142.251.242.41                             20.0%     6   18.1  18.0  17.1  18.5   0.6
 8. 108.170.250.209                            80.0%     6   17.2  17.2  17.2  17.2   0.0
 9. 108.170.234.103                            20.0%     5   18.1  17.6  16.9  18.1   0.5
10. waw02s16-in-f14.1e100.net                  75.0%     5   16.2  16.2  16.2  16.2   0.0

Comparing the two mtr command outputs, we can see that there are significant differences in the network path and performance between the two network interfaces enp0s3 and enp0s8.

In the first example using enp0s3, there is no packet loss and the average round-trip time is around 5.9 ms. This suggests that the network path is relatively stable and performing well.

In contrast, the second example using enp0s8 shows significant packet loss at several points along the network path, with an average round-trip time of 9.6 ms. This indicates that there may be issues with the network connectivity between the host and the destination.


ubuntu@ip-172-31-42-130:~$ mtr -I enp0s3 -T google.com
                                  My traceroute  [v0.95]
ip-172-31-42-130 (10.0.2.13) -> google.com (216.58.215.78)       2023-08-09T14:17:12+0000
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                                 Packets               Pings
 Host                                          Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. waw02s16-in-f14.1e100.net                   0.0%     8   20.7  20.0  17.5  29.5   4.0

ubuntu@ip-172-31-42-130:~$ mtr -I enp0s8 -T google.com
                                   My traceroute  [v0.95]
ip-172-31-42-130 (192.168.0.77) -> google.com (216.58.215.78)    2023-08-09T14:18:11+0000
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                                 Packets               Pings
 Host                                          Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. _gateway                                   87.5%    33    2.6   3.1   2.0   4.6   1.1
 2. 254.20.150.178.triolan.net                 96.9%    33    7.2   7.2   7.2   7.2   0.0
 3. 10.131.109.109                             87.1%    32    4.7   4.2   2.6   4.9   1.1
 4. 226.3.86.109.triolan.net                   96.8%    32   17.3  17.3  17.3  17.3   0.0
 5. 108.170.248.138                            87.1%    32    3.4   3.2   2.8   3.4   0.3
    108.170.248.155
 6. 142.251.242.39                             96.8%    32   21.7  21.7  21.7  21.7   0.0
 7. 108.170.250.209                            87.1%    32   17.6  17.3  16.4  17.7   0.6
    142.251.242.37
    108.170.250.193
 8. 108.170.250.193                            96.8%    32   28.2  28.2  28.2  28.2   0.0
 9. 108.170.234.103                            87.1%    32   18.3  17.6  16.3  18.4   1.0
    waw02s16-in-f14.1e100.net
    108.170.234.101
10. waw02s16-in-f14.1e100.net                  96.7%    31   25.3  25.3  25.3  25.3   0.0

Comparing the two mtr command outputs with the -T option, we can see that there are significant differences in the network path and performance between the two network interfaces enp0s3 and enp0s8.

In the first example using enp0s3, the output shows that the network path involves a single router, waw02s16-in-f14.1e100.net, and there is no packet loss occurring. The average round-trip time for TCP packets is around 20 ms, which is slightly higher than the average round-trip time of 5.9 ms when using ICMP packets in the previous example.

In contrast, the second example using enp0s8 shows significant packet loss occurring at several points along the network path, with some routers dropping as much as 96.9% of the packets. The average round-trip time for TCP packets is also much higher than in the previous example using enp0s3, with an average of 3.2 ms.

ubuntu@ip-172-31-42-130:~$ mtr -I enp0s3 -u google.com
                                   My traceroute  [v0.95]
ip-172-31-42-130 (10.0.2.13) -> google.com (216.58.215.78)       2023-08-09T14:29:02+0000
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                                 Packets               Pings
 Host                                          Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. _gateway                                   92.0%    25    0.5   0.6   0.5   0.7   0.1
 2. (waiting for reply)

ubuntu@ip-172-31-42-130:~$ mtr -I enp0s8 -u google.com
                                   My traceroute  [v0.95]
ip-172-31-42-130 (192.168.0.77) -> google.com (216.58.215.78)    2023-08-09T14:28:01+0000
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                                 Packets               Pings
 Host                                          Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. _gateway                                   68.8%    17    1.7   3.0   1.7   6.7   2.1
 2. (waiting for reply)
 3. 10.131.109.109                             68.8%    17    8.2   7.5   2.8  17.9   6.2
 4. (waiting for reply)
 
Comparing the two mtr command outputs with the -u option, we can see that there are significant similarities in the network path and performance between the two network interfaces enp0s3 and enp0s8.

In both examples, there is significant packet loss occurring at the first hop, with the _gateway router dropping a high percentage of the packets. This suggests that there may be issues with the network configuration or connectivity for both interfaces.

The output also shows that there is no response from some of the hops, indicating that the packets are not reaching the destination.

ubuntu@ip-172-31-42-130:~$ traceroute google.com
traceroute to google.com (216.58.215.78), 64 hops max
  1   192.168.0.1  2.598ms  10.0.2.1  0.521ms  192.168.0.1  1.330ms
  2   *  178.150.20.254  3.453ms  *
  3   *  *  *
  4   *  *  *
  5   *  *  *
  6   *  142.251.242.35  16.156ms  *
  7   216.239.35.133  17.155ms  *  *
  8   *  *  *
  9   *  *  *
 10   *  *  *
 11   216.58.215.78  16.451ms  *  17.688ms

ubuntu@ip-172-31-42-130:~$ traceroute -I google.com
traceroute to google.com (216.58.215.78), 64 hops max
  1   10.0.2.1  0.245ms  192.168.0.1  1.440ms  10.0.2.1  0.261ms
  2   178.150.20.254  2.849ms  192.168.0.1  3.722ms  178.150.20.254  10.906ms
  3   178.150.20.254  2.655ms  10.131.109.109  16.259ms  178.150.20.254  3.390ms
  4   109.86.3.226  7.478ms  10.131.109.109  3.841ms  109.86.3.226  3.354ms
  5   109.86.3.226  3.805ms  108.170.248.138  3.467ms  109.86.3.226  4.035ms
  6   72.14.239.111  2.846ms  108.170.248.138  6.797ms  72.14.239.111  3.213ms
  7   72.14.239.111  4.335ms  142.251.242.41  15.910ms  72.14.239.111  13.660ms
  8   108.170.250.209  18.415ms  142.251.242.41  17.452ms  108.170.250.209  18.165ms
  9   108.170.250.209  17.831ms  108.170.234.103  27.673ms  108.170.250.209  20.471ms
 10   216.58.215.78  16.129ms  108.170.234.103  17.675ms  216.58.215.78  15.762ms

The output shows that the UDP/ICMP packets are passing through several routers before reaching the destination, with some routers not responding to the traceroute packets. The round-trip time for each hop varies, with some hops taking longer than others. 

    4. MTU Study:

    a. Obtain the MTU values of the local interfaces. b. Modify the MTU value of the local interfaces and determine the permissible MTU values. The permissible MTU values will be marked on the communication channel. c. Enable Jumbo Frame mode and model the advantages and disadvantages. Adjust the MTU between two virtual machines. d. * Form teams of three students. Two team members will change the MTU on their virtual machines without communicating the changes to the third team member. The third team member must calculate the MTU of the communication channel. The calculation process must be described. All team members must write their own MTU lookup script and perform an MTU lookup. e. Modify the length of the transmission queue and simulate its operation after the changes. Make several changes.
	
ubuntu@ip-172-31-42-130:~$ ip a | grep -oP '^\d+:\s+\S+.*?mtu\s+\K\d+'
65536
1500
1500

ubuntu@ip-172-31-42-130:~$ sudo ip link set dev enp0s3 mtu 1200
ubuntu@ip-172-31-42-130:~$ sudo ip link set dev enp0s8 mtu 1100	

ubuntu@ip-172-31-42-130:~$ ip a | grep -oP '^\d+:\s+\S+.*?mtu\s+\K\d+'
65536
1200
1100

ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -M do -s 1172 -c 1 google.com
PING google.com (216.58.215.78) from 10.0.2.13 enp0s3: 1172(1200) bytes of data.
76 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=1 ttl=117 (truncated)

--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 17.340/17.340/17.340/0.000 ms

ubuntu@ip-172-31-42-130:~$ ping -I enp0s8 -M do -s 1072 -c 1 google.com
PING google.com (216.58.215.78) from 192.168.0.77 enp0s8: 1072(1100) bytes of data.
76 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=1 ttl=118 (truncated)

--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 18.879/18.879/18.879/0.000 ms

ubuntu@ip-172-31-42-130:~$ sudo ip link set dev enp0s3 mtu 9000
ubuntu@ip-172-31-42-130:~$ sudo ip link set dev enp0s8 mtu 9000
[ec2-user@amazonlinux ~]$ sudo ip link set dev eth0 mtu 9000
[ec2-user@amazonlinux ~]$ sudo ip link set dev eth1 mtu 9000

[ec2-user@amazonlinux ~]$ hostname -I
10.0.2.8 192.168.0.73

ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -M do -s 8972 -c 1 192.168.0.73
PING 192.168.0.73 (192.168.0.73) from 10.0.2.13 enp0s3: 8972(9000) bytes of data.

--- 192.168.0.73 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

The issue above with sending larger packets is likely due to the presence of a router along the network path.

When sending a ping packet with a large size (e.g., 8972 bytes) and the "Don't Fragment" (DF) flag is set using the -M do option, routers along the path are not allowed to fragment the packet if the MTU of any link is smaller than the packet size. This is because the DF flag instructs routers to drop the packet instead of fragmenting it.

ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -M do -s 1472 -c 1 192.168.0.73
PING 192.168.0.73 (192.168.0.73) from 10.0.2.13 enp0s3: 1472(1500) bytes of data.
1480 bytes from 192.168.0.73: icmp_seq=1 ttl=254 time=1.90 ms

--- 192.168.0.73 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.897/1.897/1.897/0.000 ms
ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -M do -s 8972 -c 1 10.0.2.8
PING 10.0.2.8 (10.0.2.8) from 10.0.2.13 enp0s3: 8972(9000) bytes of data.
8980 bytes from 10.0.2.8: icmp_seq=1 ttl=255 time=1.16 ms

--- 10.0.2.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.156/1.156/1.156/0.000 ms
ubuntu@ip-172-31-42-130:~$ ping -I enp0s8 -M do -s 8972 -c 1 10.0.2.8
PING 10.0.2.8 (10.0.2.8) from 192.168.0.77 enp0s8: 8972(9000) bytes of data.

--- 10.0.2.8 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

The ping command failed with a 100% packet loss because there is no route to the network 10.0.2.0/24, which is the network that the IP address 10.0.2.8 belongs to.

ubuntu@ip-172-31-42-130:~$ ping -I enp0s8 -M do -s 8972 -c 1 192.168.0.73
PING 192.168.0.73 (192.168.0.73) from 192.168.0.77 enp0s8: 8972(9000) bytes of data.
8980 bytes from 192.168.0.73: icmp_seq=1 ttl=255 time=1.06 ms

--- 192.168.0.73 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.058/1.058/1.058/0.000 ms


ubuntu@ip-172-31-42-130:~$ mtufinder() { for i in $(seq $2 $3); do if ! ping -I $1 -M do -s $i $4 -c 1 -i 0.2 >/dev/null 2>&1; then echo "Maximum MTU size is: $(($(($i-1)) + 28))"; ping -I $1 -M do -s $(($i-1)) $4 -c 1 -i 0.2; break; fi; done; }; mtufinder enp0s8 1000 9000 192.168.0.7
3
Maximum MTU size is: 9000
PING 192.168.0.73 (192.168.0.73) from 192.168.0.77 enp0s8: 8972(9000) bytes of data.
8980 bytes from 192.168.0.73: icmp_seq=1 ttl=255 time=1.29 ms

--- 192.168.0.73 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.287/1.287/1.287/0.000 ms

The command defines a bash function called mtufinder() that tests the MTU size between the minimum and maximum values for a specified network interface and IP address.

The function uses the ping command with the -M do option to test the MTU size. If the ping command fails, the function outputs the MTU size that was successful and then pings the machine with that MTU size. It then breaks the loop.

The output of the command shows that the maximum MTU size for the network interface "enp0s8" and the machine with IP address 192.168.0.73 is 9000 bytes. The function then pings the machine with that MTU size and receives a successful response.

ubuntu@ip-172-31-42-130:~$ sysctl net.core.netdev_max_backlog
net.core.netdev_max_backlog = 1000

Showing the current value of the transmission queue length parameter.

ubuntu@ip-172-31-42-130:~$ sudo sysctl -w net.core.netdev_max_backlog=3000
net.core.netdev_max_backlog = 3000

ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -i 0.2 -c 4 10.0.2.8
PING 10.0.2.8 (10.0.2.8) from 10.0.2.13 enp0s3: 56(84) bytes of data.
64 bytes from 10.0.2.8: icmp_seq=1 ttl=255 time=0.572 ms
64 bytes from 10.0.2.8: icmp_seq=2 ttl=255 time=0.738 ms
64 bytes from 10.0.2.8: icmp_seq=3 ttl=255 time=0.689 ms
64 bytes from 10.0.2.8: icmp_seq=4 ttl=255 time=0.773 ms

--- 10.0.2.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 604ms
rtt min/avg/max/mdev = 0.572/0.693/0.773/0.075 ms

ubuntu@ip-172-31-42-130:~$ sudo sysctl -w net.core.netdev_max_backlog=1
net.core.netdev_max_backlog = 1
ubuntu@ip-172-31-42-130:~$ ping -I enp0s3 -i 0.2 -c 4 10.0.2.8
PING 10.0.2.8 (10.0.2.8) from 10.0.2.13 enp0s3: 56(84) bytes of data.
64 bytes from 10.0.2.8: icmp_seq=1 ttl=255 time=1.01 ms
64 bytes from 10.0.2.8: icmp_seq=2 ttl=255 time=1.35 ms
64 bytes from 10.0.2.8: icmp_seq=3 ttl=255 time=1.84 ms
64 bytes from 10.0.2.8: icmp_seq=4 ttl=255 time=1.25 ms

--- 10.0.2.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 606ms
rtt min/avg/max/mdev = 1.006/1.362/1.836/0.301 ms

No impact.


    5. Learning MAC: a. Identify all MAC addresses available in your network, including those of colleague hosts and resources. b. Utilize the arp and ip commands to accomplish this task. c. Develop a system for automatic detection of changes, such as the appearance or shutdown of a device or VM, on the local network.

ubuntu@ip-172-31-42-130:~$ arp -a
_gateway (10.0.2.1) at 52:54:00:12:35:00 [ether] on enp0s3
? (192.168.0.220) at 7c:64:56:1c:57:4d [ether] on enp0s8
? (192.168.0.80) at 18:f4:6a:0c:cb:d1 [ether] on enp0s8
? (192.168.0.73) at 08:00:27:a8:3e:b7 [ether] on enp0s8
_gateway (192.168.0.1) at 28:87:ba:c4:96:a0 [ether] on enp0s8
ubuntu@ip-172-31-42-130:~$ ip n
10.0.2.1 dev enp0s3 lladdr 52:54:00:12:35:00 STALE
192.168.0.220 dev enp0s8 lladdr 7c:64:56:1c:57:4d STALE
192.168.0.80 dev enp0s8 lladdr 18:f4:6a:0c:cb:d1 DELAY
192.168.0.73 dev enp0s8 lladdr 08:00:27:a8:3e:b7 STALE
192.168.0.1 dev enp0s8 lladdr 28:87:ba:c4:96:a0 STALE

The first line of the "arp -a" output shows the MAC address of the default gateway, which is the device that connects your network to the internet. The other lines show the MAC addresses of devices connected to the network, along with their IP addresses.

The "ip n" command shows the same information as the "arp -a" command, but in a different format. It shows the IP addresses and MAC addresses of devices connected to the network, along with the interface they are connected to.

The "STALE" and "DELAY" entries in the output indicate the status of the ARP cache entry. "STALE" means that the entry is still valid, but it has not been used recently. "DELAY" means that the entry is still valid, but it is being refreshed.

#!/bin/bash

# Global vars
script_name=$(basename -- "$0")

# Set the path to the files where the LAN state will be stored
LAN_STATE_FILE="/tmp/$script_name.lan_state.txt"
LAN_STATE_FILE_UPDATE="/tmp/$script_name.lan_state_update.txt"

# Scan networks with nmap function
nmap_scan() {
    for ip in $(nmap -sn "$@" 2>/dev/null |
    grep -E [0-9]\)?*$ | awk '{print $NF}'); do
        echo "$ip"
    done
}

# Network monitoring function
netmonitor() {
    # Retrieve the current LAN state and save it to the file
    nmap_scan "$@" > "$LAN_STATE_FILE"

    # Loop indefinitely
    while true; do
        # Wait for 10 seconds
        sleep 10

        # Retrieve the current LAN state
        nmap_scan "$@" > "$LAN_STATE_FILE_UPDATE"

        # Compare the new LAN state to the stored LAN state
        diff_output=$(diff "$LAN_STATE_FILE" "$LAN_STATE_FILE_UPDATE")

        if [ -n "$diff_output" ]; then
            # If there is a difference, notify the user
            if [ $(echo "$@" | wc -w) -eq 1 ]; then
                echo "LAN state has changed on the network at $(date)"
            else
                echo "LAN state has changed on the networks at $(date)"
            fi
            while IFS=' ' read -r mark ip; do
                if [[ "$mark" == "<" ]]; then
                    echo "Host: $ip shut down"
                    # You can add your notification method here, such as sending an email or SMS
                elif [[ "$mark" == ">" ]]; then
                    echo "Host: $ip started up"
                    # You can add your notification method here, such as sending an email or SMS
                fi
            done <<< "$diff_output"
        fi	

        # Replace the stored LAN state with the new LAN state
        mv "$LAN_STATE_FILE_UPDATE" "$LAN_STATE_FILE"
    done
}

# Check if nmap is installed
if ! command -v nmap &> /dev/null; then
    echo "nmap could not be found. Please install nmap and try again."
    exit 1
fi

# Check if the network argument is provided
if [ -z "$1" ]; then
    echo "Please provide a network argument, such as 192.168.1.0/24."
    exit 1
fi

# Start the network monitoring function
netmonitor "$@"

In this script, the netmonitor function is used to monitor the LAN state of one or more networks using the nmap_scan function. The nmap_scan function uses the nmap command to scan the specified network(s) and retrieve the list of IP addresses. The netmonitor function then compares the current LAN state to the stored LAN state and displays a message if there are any changes.

The if condition checks if the network argument is provided and displays an error message if it is not. The netmonitor function is then called with the network argument(s) passed to it.

./netmonitor.sh $(ip a | grep 'inet ' | cut -d ' ' -f 6 | sed -n '2p')
LAN state has changed on the network at Thu Aug 10 05:51:48 PM UTC 2023
Host: 10.0.2.8 started up
LAN state has changed on the network at Thu Aug 10 05:54:52 PM UTC 2023
Host: 10.0.2.8 shut down

[ec2-user@amazonlinux ~]$ hostname -I && echo $(date)
10.0.2.8 192.168.0.73
Thu Aug 10 17:54:00 UTC 2023
[ec2-user@amazonlinux ~]$ sudo shutdown now

    6. In this section, we will cover network administration in Linux, specifically focusing on static configuration of the network interface for Ubuntu 22.04, Debian 12, Oracle Linux 9, and CentOS 9. We will cover the following topics:

    Setting a temporary static IP address
    Setting a permanent static IP address
    Setting a static IP address with the minimum allowable mask for the network based on the number of computers (using 2^(<last number of your ID pass> or <date of birth>, and if the number is 0 or 1, then take the following)
    Assigning multiple IP addresses to one link layer interface
    Ways to change the MAC address in operating systems and installing a locally administered MAC address (including finding where it's used)
    Getting the list of MAC addresses for multicast
    Verifying changes made with the ip and ipconfig (ifconfig) command

    We will also cover how to configure the gateway address, including how gateways work with multiple interfaces. Additionally, we will discuss the assignment of mask for host and for router. Finally, we will learn how to get a list of network protocols and their versions supported by the system kernel.
	
ubuntu@ip-172-31-42-130:~$ sudo ip a a 10.0.2.22/24 dev enp0s3
ubuntu@ip-172-31-42-130:~$ hostname -I
10.0.2.13 10.0.2.22 192.168.0.77
ubuntu@ip-172-31-42-130:~$ nmap -sn 10.0.2.22/24
Starting Nmap 7.80 ( https://nmap.org ) at 2023-08-10 20:04 UTC
Nmap scan report for _gateway (10.0.2.1)
Host is up (0.0032s latency).
Nmap scan report for 10.0.2.8
Host is up (0.0010s latency).
Nmap scan report for ip-172-31-42-130 (10.0.2.13)
Host is up (0.0011s latency).
Nmap scan report for ip-172-31-42-130 (10.0.2.22)
Host is up (0.00022s latency).
Nmap done: 256 IP addresses (4 hosts up) scanned in 2.94 seconds
	
[ec2-user@amazonlinux ~]$ ping -c 2 10.0.2.22
PING 10.0.2.22 (10.0.2.22) 56(84) bytes of data.
64 bytes from 10.0.2.22: icmp_seq=1 ttl=64 time=2.87 ms
64 bytes from 10.0.2.22: icmp_seq=2 ttl=64 time=1.11 ms

--- 10.0.2.22 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1005ms
rtt min/avg/max/mdev = 1.119/1.995/2.871/0.876 ms

ubuntu@ip-172-31-42-130:~$ sudo nano /etc/netplan/00-installer-config.yaml
# This is the network config written by 'subiquity'
network:
  ethernets:
    enp0s3:
      dhcp4: no
      addresses: [10.0.2.26/24]
      routes:
        - to: default
          via: 10.0.2.1
      nameservers:
        addresses: [8.8.8.8]
    enp0s8:
      dhcp4: no
      addresses: [192.168.0.111/24]
      nameservers:
        addresses: [8.8.8.8]
  version: 2
ubuntu@ip-172-31-42-130:~$ sudo netplan apply
ubuntu@ip-172-31-42-130:~$ hostname -I
10.0.2.26 192.168.0.111

[a@lan ~]$ cat /etc/centos-release
CentOS Linux release 7.9.2009 (Core)
[a@lan ~]$ hostname -I
10.0.2.15 192.168.0.40 192.168.122.1
[a@lan ~]$ sudo ip a a 10.0.2.212/24 dev enp0s3
[a@lan ~]$ hostname -I
10.0.2.15 10.0.2.212 192.168.0.40 192.168.122.1

[a@lan ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s3
UUID=768724f3-4bb1-413b-8bac-fbcb0230469e
DEVICE=enp0s3
ONBOOT=yes
NM_CONTROLLED=no
NETWORK=10.0.2.0
IPADDR=10.0.2.225
NETMASK=255.255.255.0
BROADCAST=10.0.2.255
DNS1=10.0.2.1
GATEWAY=10.0.2.1
  
[a@lan ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
NM_CONTROLLED=no
NETWORK="192.168.0.0"
IPADDR=192.168.0.240
NETMASK=255.255.255.0
BROADCAST=192.168.0.255
DNS1=192.168.0.1
[a@lan ~]$ sudo chkconfig NetworkManager off
[a@lan ~]$ sudo chkconfig network on
[a@lan ~]$ sudo service NetworkManager stop
[a@lan ~]$ sudo service network force-reload
[a@lan ~]$ hostname -I
10.0.2.225 192.168.0.240 192.168.122.1

[ec2-user@amazonlinux ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-eth0
# Created by cloud-init on instance boot automatically, do not edit.
#
BOOTPROTO=none
DEVICE=eth0
HWADDR=08:00:27:6f:73:c1
NM_CONTROLLED=no
ONBOOT=yes
STARTMODE=auto
TYPE=Ethernet
IPADDR=10.0.2.200
USERCTL=no
NETWORK=10.0.2.0
NETMASK=255.255.255.0
GATEWAY=10.0.2.1
[ec2-user@amazonlinux ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-eth1
# Created by cloud-init on instance boot automatically, do not edit.
#
BOOTPROTO=none
DEVICE=eth1
HWADDR=08:00:27:a8:3e:b7
NM_CONTROLLED=no
ONBOOT=yes
STARTMODE=auto
TYPE=Ethernet
IPADDR=192.168.0.222
USERCTL=no
NETWORK=192.168.0.0
NETMASK=255.255.255.0
GATEWAY=192.168.0.1
[ec2-user@amazonlinux ~]$ sudo service network force-reload
[ec2-user@amazonlinux ~]$ hostname -I
10.0.2.200 192.168.0.222
[ec2-user@amazonlinux ~]$ sudo ip a a 10.0.2.110/24 dev eth0
[ec2-user@amazonlinux ~]$ hostname -I
10.0.2.200 10.0.2.110 192.168.0.222 192.168.0.73

ubuntu@ip-172-31-42-130:~$ sudo nano /etc/netplan/00-installer-config.yaml
### more
    enp0s3:
      dhcp4: no
      addresses: [10.0.2.26/28] # 2^4=16; mask = 32 - log2(14) = 28
      routes:
        - to: default
          via: 10.0.2.1
### more
ubuntu@ip-172-31-42-130:~$ sudo netplan apply
ubuntu@ip-172-31-42-130:~$ ip a s enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:41:52:ce brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.26/28 brd 10.0.2.31 scope global enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe41:52ce/64 scope link
       valid_lft forever preferred_lft forever	

ubuntu@ip-172-31-42-130:~$ for i in {27..30}; do sudo ip a a 10.0.2.$i/24 dev enp0s3; done
ubuntu@ip-172-31-42-130:~$ hostname -I
10.0.2.13 10.0.2.27 10.0.2.28 10.0.2.29 10.0.2.30 192.168.0.77
ubuntu@ip-172-31-42-130:~$ ip a s enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:41:52:ce brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.13/24 metric 100 brd 10.0.2.255 scope global dynamic enp0s3
       valid_lft 545sec preferred_lft 545sec
    inet 10.0.2.27/24 scope global secondary enp0s3
       valid_lft forever preferred_lft forever
    inet 10.0.2.28/24 scope global secondary enp0s3
       valid_lft forever preferred_lft forever
    inet 10.0.2.29/24 scope global secondary enp0s3
       valid_lft forever preferred_lft forever
    inet 10.0.2.30/24 scope global secondary enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe41:52ce/64 scope link
       valid_lft forever preferred_lft forever   
ubuntu@ip-172-31-42-130:~$ ip a s enp0s3 | grep -oP '(?<=link/ether )\S+'
08:00:27:41:52:ce 
ubuntu@ip-172-31-42-130:~$ sudo ip l s dev enp0s3 a 00:11:22:33:44:55
ubuntu@ip-172-31-42-130:~$ ip a s enp0s3 | grep -oP '(?<=link/ether )\S+'
00:11:22:33:44:55

Locally administered MAC addresses are used in situations where a device needs to have a unique MAC address that is not tied to a specific manufacturer. This can be useful in a variety of scenarios, such as:

    Virtualization: When creating virtual machines, it is often necessary to assign a unique MAC address to each virtual network interface. Locally administered MAC addresses can be used to ensure that each virtual interface has a unique MAC address.

    Testing: Locally administered MAC addresses can be used in testing environments to simulate multiple devices on a network without having to purchase additional hardware.

    Security: In some cases, it may be desirable to use a MAC address that is not associated with a specific manufacturer to avoid being tracked or identified by network administrators or other devices on the network.

    Troubleshooting: Locally administered MAC addresses can be used to troubleshoot network connectivity issues by allowing a device to assume the MAC address of another device on the network.

ubuntu@ip-172-31-42-130:~$ ip m s dev enp0s3
2:      enp0s3
        link  33:33:00:00:00:01
        link  01:00:5e:00:00:01
        link  33:33:ff:41:52:ce
        link  01:80:c2:00:00:00
        link  01:80:c2:00:00:03
        link  01:80:c2:00:00:0e
        inet  224.0.0.1
        inet6 ff02::1:ff41:52ce
        inet6 ff02::1 users 2
        inet6 ff01::1

ubuntu@ip-172-31-42-130:~$ sudo nano /etc/netplan/00-installer-config.yaml
network:
  version: 2
  ethernets:
    enp0s3:
      dhcp4: true
    enp0s8:
      addresses: [192.168.0.77/24]
      routes:
        - to: default
          via: 192.168.0.1
      nameservers:
        addresses: [192.168.0.1]
      dhcp4: no
    enp0s9:
      addresses: [10.5.5.1/27]
      nameservers:
        addresses: [192.168.0.1]
      dhcp4: no	
ubuntu@ip-172-31-42-130:~$ sudo netplan apply
ubuntu@ip-172-31-42-130:~$ sudo nano /etc/sysctl.conf
net.ipv4.ip_forward=1

ubuntu@ip-172-31-42-130:~$ sudo nano /etc/default/isc-dhcp-server
INTERFACESv4="enp0s9"
INTERFACESv6="enp0s9"	
ubuntu@ip-172-31-42-130:~$ sudo nano /etc/dhcp/dhcpd.conf  
# A slightly different configuration for an internal subnet.
subnet 10.5.5.0 netmask 255.255.255.224 {
  range 10.5.5.26 10.5.5.30;
  option domain-name-servers ns1.internal.ip-172-31-42-130.org;
  option domain-name "internal.ip-172-31-42-130.org";
  option subnet-mask 255.255.255.224;
  option routers 10.5.5.1;
  option broadcast-address 10.5.5.31;
  default-lease-time 600;
  max-lease-time 7200;
}
ubuntu@ip-172-31-42-130:~$ sudo systemctl restart isc-dhcp-server
ubuntu@ip-172-31-42-130:~$ sudo iptables -t nat -A POSTROUTING -o enp0s8 -j MASQUERADE
ubuntu@ip-172-31-42-130:~$ sudo iptables -t nat -L
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination

Chain INPUT (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination
MASQUERADE  all  --  anywhere             anywhere

the current configuration of the NAT (Network Address Translation) table in the iptables firewall on an Ubuntu server.

The output shows four chains: PREROUTING, INPUT, OUTPUT, and POSTROUTING. These chains are used to define rules for packets as they pass through the firewall.

    PREROUTING: This chain is used to modify packets as they come into the server, before they are processed by the firewall rules.
    INPUT: This chain is used to process packets that are destined for the server itself.
    OUTPUT: This chain is used to process packets that are generated by the server itself.
    POSTROUTING: This chain is used to modify packets as they leave the server, after they have been processed by the firewall rules.

In this case, all the chains have a policy of ACCEPT, which means that by default, all packets are allowed to pass through the firewall. The last line in the output shows a MASQUERADE rule in the POSTROUTING chain, which is used to modify the source IP address of outgoing packets to match the IP address of the server's network interface. This is typically used when the server is acting as a gateway or router for other devices on the network.

ubuntu@ip-172-31-42-130:~$ ip r s
default via 192.168.0.1 dev enp0s8 proto static
default via 10.0.2.1 dev enp0s3 proto dhcp src 10.0.2.13 metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.13 metric 100
10.0.2.1 dev enp0s3 proto dhcp scope link src 10.0.2.13 metric 100
10.5.5.0/27 dev enp0s9 proto kernel scope link src 10.5.5.1
192.168.0.0/24 dev enp0s8 proto kernel scope link src 192.168.0.77
192.168.0.1 via 10.0.2.1 dev enp0s3 proto dhcp src 10.0.2.13 metric 100

default via 192.168.0.1 dev enp0s8 proto static

This line specifies the default route for the system. It indicates that any traffic that does not match a more specific route should be sent to the gateway at IP address 192.168.0.1 via the enp0s8 network interface.

default via 10.0.2.1 dev enp0s3 proto dhcp src 10.0.2.13 metric 100

This line specifies a second default route for the system. It indicates that any traffic that does not match a more specific route should be sent to the gateway at IP address 10.0.2.1 via the enp0s3 network interface. This route was added by the DHCP client on the system, which obtained the default gateway address from the DHCP server.

10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.13 metric 100

This line specifies a route for the 10.0.2.0/24 network. It indicates that any traffic destined for the 10.0.2.0/24 network should be sent via the enp0s3 network interface.

10.0.2.1 dev enp0s3 proto dhcp scope link src 10.0.2.13 metric 100

This line specifies a route for the gateway at IP address 10.0.2.1. It indicates that any traffic destined for the gateway should be sent via the enp0s3 network interface.

10.5.5.0/27 dev enp0s9 proto kernel scope link src 10.5.5.1

This line specifies a route for the 10.5.5.0/27 network. It indicates that any traffic destined for the 10.5.5.0/27 network should be sent via the enp0s9 network interface.

192.168.0.0/24 dev enp0s8 proto kernel scope link src 192.168.0.77

This line specifies a route for the 192.168.0.0/24 network. It indicates that any traffic destined for the 192.168.0.0/24 network should be sent via the enp0s8 network interface.

192.168.0.1 via 10.0.2.1 dev enp0s3 proto dhcp src 10.0.2.13 metric 100

This line specifies a route for the gateway at IP address 192.168.0.1. It indicates that any traffic destined for the gateway should be sent via the enp0s3 network interface. This route was added by the DHCP client on the system, which obtained the gateway address from the DHCP server.
[ec2-user@amazonlinux ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-eth2
BOOTPROTO=dhcp
DEVICE=eth2
HWADDR=08:00:27:b1:8b:27
NM_CONTROLLED=no
ONBOOT=yes
STARTMODE=auto
TYPE=Ethernet
USERCTL=no
NETWORK=10.5.5.0
NETMASK=255.255.255.224
[ec2-user@amazonlinux ~]$ sudo service network force-reload
[ec2-user@amazonlinux ~]$ ip a s dev eth2
4: eth2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:b1:8b:27 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.26/27 brd 10.5.5.31 scope global dynamic eth2
       valid_lft 512sec preferred_lft 512sec
    inet6 fe80::a00:27ff:feb1:8b27/64 scope link
       valid_lft forever preferred_lft forever

[ec2-user@amazonlinux ~]$ ping -I eth2 -c 2 google.com
PING google.com (216.58.215.78) from 10.5.5.26 eth2: 56(84) bytes of data.
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=1 ttl=117 time=46.0 ms
64 bytes from waw02s16-in-f14.1e100.net (216.58.215.78): icmp_seq=2 ttl=117 time=42.1 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 42.130/44.088/46.046/1.958 ms

Both packets were successfully transmitted and received without any packet loss. The round-trip time (RTT) for each packet is also displayed, which is the time it took for the packet to travel from the source to the destination and back again.

The statistics at the end of the output show the minimum, average, maximum, and standard deviation of the RTT values for the two packets. In this case, the minimum RTT was 42.130 ms, the average RTT was 44.088 ms, the maximum RTT was 46.046 ms, and the standard deviation was 1.958 ms. 	 
  
ubuntu@ip-172-31-42-130:~$ cat /proc/net/protocols | awk '{if(NR>1) print $1,$2}'
PACKET 1600
MPTCPv6 2016
PINGv6 1200
RAWv6 1200
UDPLITEv6 1344
UDPv6 1344
TCPv6 2392
XDP 1024
UNIX-STREAM 1088
UNIX 1088
UDP-Lite 1152
MPTCP 1856
PING 992
RAW 1000
UDP 1152
TCP 2232
NETLINK 1128

    1. Perform installation in Windows and Linux operating systems:
        a. Wireshark
        b. tcpdump
    2. Capture network traffic on your host: a. Identify Ethernet frames: i. Identify unicast frames and determine their source and destination. ii. Identify broadcast frames and determine the service that sends/receives them. iii. Simulate the operation of the ARP protocol to intercept traffic. b. Identify IP packets: i. Determine the application layer service to which they belong and the type of traffic. ii. Identify incoming and outgoing IP packets and explain the captured traffic. iii. Identify packets that are unicast, broadcast, or multicast. iv. Identify packets that confirm IP fragmentation on Ethernet and determine the size of the packages received by the recipient and the number of packages. v. Transfer large packets and identify fragmented packets. Analyze fragmentation for incoming and outgoing packets. c. Identify TCP segments: i. Identify TCP segments that confirm the connection establishment process (handshaking). ii. Identify TCP segments that confirm the connection data transfer process (ESTABLISHED). 	ii. Identify TCP segments that confirm client and server communication in the established connection state (ESTABLISHED), but without data transfer. iv. Identify TCP segments that confirm the connection termination process. v. Transfer large TCP/UDP segments and calculate the Maximum Segment Size (MSS). Review all large segment transfer traffic at the lower layers of the TCP/IP model. vi. * Identify TCP segments that confirm the termination of traffic transmission due to recipient buffer overflow (describe the verification process). vii. * Identify TCP segments that confirm the retransmission of segments due to their loss during transmission (describe the verification process). d. Identify the following protocols using filters: i. DNS (UDP/TCP datagrams) ii. DHCP (UDP/TCP datagrams) iii. HTTP (TCP segments) iv. * HTTPS (TCP segments) v. * TLS (decrypt encrypted traffic)	

[ec2-user@amazonlinux ~]$ iperf -c 224.0.0.1 -u -T 32 -n 200 -l 100 -B 10.5.5.26
------------------------------------------------------------
Client connecting to 224.0.0.1, UDP port 5001
Binding to local address 10.5.5.26
Sending 100 byte datagrams, IPG target: 762.94 us (kalman adjust)
Setting multicast TTL to 32
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  3] local 10.5.5.26 port 45031 connected with 224.0.0.1 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0- 0.0 sec   200 Bytes  0.00 bits/sec
[  3] Sent 2 datagrams

ubuntu@ip-172-31-42-130:~$ sudo tshark -i enp0s9 -f "udp and dst host 224.0.0.1 and src host 10.5.5.26"
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp0s9'
 ** (tshark:6444) 21:49:09.824262 [Main MESSAGE] -- Capture started.
 ** (tshark:6444) 21:49:09.825075 [Main MESSAGE] -- File: "/tmp/wireshark_enp0s9SAWP91.pcapng"
    1 0.000000000    10.5.5.26 → 224.0.0.1    UDP 142 45031 → 5001 Len=100
    2 0.001661001    10.5.5.26 → 224.0.0.1    UDP 142 45031 → 5001 Len=100
    3 0.001661192    10.5.5.26 → 224.0.0.1    UDP 142 45031 → 5001 Len=100
^Ctshark:
3 packets captured

[ec2-user@amazonlinux ~]$ ping -I eth2 -c 2 -b 10.5.5.31
WARNING: pinging broadcast address
PING 10.5.5.31 (10.5.5.31) from 10.5.5.26 eth2: 56(84) bytes of data.

--- 10.5.5.31 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1027ms

ubuntu@ip-172-31-42-130:~$ sudo tshark -i enp0s9 -f "src host 10.5.5.26 and broadcast"
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp0s9'
 ** (tshark:1773) 11:31:56.495552 [Main MESSAGE] -- Capture started.
 ** (tshark:1773) 11:31:56.496277 [Main MESSAGE] -- File: "/tmp/wireshark_enp0s9H86U91.pcapng"
    1 0.000000000    10.5.5.26 → 10.5.5.31    ICMP 98 Echo (ping) request  id=0x0ea6, seq=1/256, ttl=255
    2 1.027387461    10.5.5.26 → 10.5.5.31    ICMP 98 Echo (ping) request  id=0x0ea6, seq=2/512, ttl=255
^Ctshark:
2 packets captured

[ec2-user@amazonlinux ~]$ sudo arping -U -I eth2 -c 2 10.5.5.26
ARPING 10.5.5.26 from 10.5.5.26 eth2
Sent 2 probes (2 broadcast(s))
Received 0 response(s)
ubuntu@ip-172-31-42-130:~$ sudo tshark -i enp0s9 -f "src host 10.5.5.26 and broadcast"
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp0s9'
 ** (tshark:1800) 11:36:30.045135 [Main MESSAGE] -- Capture started.
 ** (tshark:1800) 11:36:30.046719 [Main MESSAGE] -- File: "/tmp/wireshark_enp0s9C1OL91.pcapng"
    1 0.000000000 PcsCompu_b1:8b:27 → Broadcast    ARP 60 Gratuitous ARP for 10.5.5.26 (Request)
    2 1.000451678 PcsCompu_b1:8b:27 → Broadcast    ARP 60 Gratuitous ARP for 10.5.5.26 (Request)
^Ctshark:
2 packets captured

[ec2-user@amazonlinux ~]$ curl -ILs --interface eth2 google.com
HTTP/1.1 301 Moved Permanently
Location: http://www.google.com/
Content-Type: text/html; charset=UTF-8
Content-Security-Policy-Report-Only: object-src 'none';base-uri 'self';script-src 'nonce-heJt9_0r0_UIncArLfW-xA' 'strict-dynamic' 'report-sample' 'unsafe-eval' 'unsafe-inline' https: http:;report-uri https://csp.withgoogle.com/csp/gws/other-hp
Date: Sun, 13 Aug 2023 12:37:49 GMT
Expires: Tue, 12 Sep 2023 12:37:49 GMT
Cache-Control: public, max-age=2592000
Server: gws
Content-Length: 219
X-XSS-Protection: 0
X-Frame-Options: SAMEORIGIN

HTTP/1.1 200 OK
Content-Type: text/html; charset=ISO-8859-1
Content-Security-Policy-Report-Only: object-src 'none';base-uri 'self';script-src 'nonce-d0gnhrkH8a2p_yBUJxHKhg' 'strict-dynamic' 'report-sample' 'unsafe-eval' 'unsafe-inline' https: http:;report-uri https://csp.withgoogle.com/csp/gws/other-hp
P3P: CP="This is not a P3P policy! See g.co/p3phelp for more info."
Date: Sun, 13 Aug 2023 12:37:49 GMT
Server: gws
X-XSS-Protection: 0
X-Frame-Options: SAMEORIGIN
Transfer-Encoding: chunked
Expires: Sun, 13 Aug 2023 12:37:49 GMT
Cache-Control: private
Set-Cookie: 1P_JAR=2023-08-13-12; expires=Tue, 12-Sep-2023 12:37:49 GMT; path=/; domain=.google.com; Secure
Set-Cookie: AEC=Ad49MVHDxLZZd0n7UVYluHy8W8KaR1b8F0nOM9xjLUxjU3Obb7i3SGeFyw; expires=Fri, 09-Feb-2024 12:37:49 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=lax
Set-Cookie: NID=511=EWu-Km6yNDqrnxoakCvNJsOE-o2ao02HaBX4-gOBpVy643ykUJM3nKmhxfcosmudyPb3XD_Aa59N_Gt-ldndFLxIjY_Cb4gNQ9P45lNgk16hnkUrs8_CUlBd7WJSYkQHudul_LZBoVn9j18ZuleWSmOwXAl7tGT7UIhKiDmZQG8; expires=Mon, 12-Feb-2024 12:37:49 GMT; path=/; domain=.google.com; HttpOnly

ubuntu@ip-172-31-42-130:~$ sudo tshark -i enp0s9 -T fields -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e udp.srcport -e udp.dstport -e http.host -e dns.qry.name
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp0s9'
 ** (tshark:2060) 12:37:42.400835 [Main MESSAGE] -- Capture started.
 ** (tshark:2060) 12:37:42.401761 [Main MESSAGE] -- File: "/tmp/wireshark_enp0s9N6OU91.pcapng"
10.5.5.26       216.58.215.78   43650   80
216.58.215.78   10.5.5.26       80      43650
10.5.5.26       216.58.215.78   43650   80
10.5.5.26       216.58.215.78   43650   80                      google.com
216.58.215.78   10.5.5.26       80      43650
216.58.215.78   10.5.5.26       80      43650
10.5.5.26       216.58.215.78   43650   80
10.5.5.26       216.58.209.4    48476   80
216.58.209.4    10.5.5.26       80      48476
10.5.5.26       216.58.209.4    48476   80
10.5.5.26       216.58.209.4    48476   80                      www.google.com
216.58.209.4    10.5.5.26       80      48476
216.58.209.4    10.5.5.26       80      48476
10.5.5.26       216.58.209.4    48476   80
10.5.5.26       216.58.209.4    48476   80
10.5.5.26       216.58.215.78   43650   80
216.58.209.4    10.5.5.26       80      48476
216.58.215.78   10.5.5.26       80      43650
10.5.5.26       216.58.209.4    48476   80
10.5.5.26       216.58.215.78   43650   80


10.5.5.26       10.5.5.1                        68      67
10.5.5.1        10.5.5.26                       67      68


^Ctshark:
26 packets captured

The output shows several pairs of IP addresses with corresponding source and destination ports. Each pair represents a communication session between two devices.

For example, the first line shows that a packet was sent from IP address 10.5.5.26 to IP address 216.58.215.78 with source port 43650 and destination port 80. This is an outbound packet from the device with IP address 10.5.5.26 to the device with IP address 216.58.215.78 on port 80.

The second line shows a packet sent in the opposite direction, from IP address 216.58.215.78 to IP address 10.5.5.26 on port 80. This is an inbound packet to the device with IP address 10.5.5.26 from the device with IP address 216.58.215.78 on port 80.

The other lines show similar pairs of inbound and outbound packets with different IP addresses and ports. The packets are  unicast.

[ec2-user@amazonlinux ~]$ sudo hping3 -c 1 -d 2000 -E -F -M -S -p 80 -I eth2 10.5.5.1
HPING 10.5.5.1 (eth2 10.5.5.1): NO FLAGS are set, 40 headers + 2000 data bytes
[main] memlockall(): Success
Warning: can't disable memory paging!
[datafiller] open(): No such file or directory
len=46 ip=10.5.5.1 ttl=64 DF id=0 sport=80 flags=RA seq=0 win=0 rtt=1.6 ms

--- 10.5.5.1 hping statistic ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 1.6/1.6/1.6 ms

root@ip-172-31-42-130:~# sudo tshark -i enp0s9 -f "host 10.5.5.26" -T fields -e ip.len -e i
p.flags.mf -w captured_traffic2.pcapng
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp0s9'
 ** (tshark:3984) 21:31:34.779089 [Main MESSAGE] -- Capture started.
 ** (tshark:3984) 21:31:34.780560 [Main MESSAGE] -- File: "captured_traffic2.pcapng"
1500    1
560     0
40      0




^Ctshark:
7 packets captured

ubuntu@ip-172-31-42-130:~$ tshark -r captured_traffic2.pcapng
    1 0.000000000    10.5.5.26 → 10.5.5.1     IPv4 1514 Fragmented IP protocol (proto=TCP 6, off=0, ID=000b)
    2 0.000000746    10.5.5.26 → 10.5.5.1     TCP 574 1388 → 80 [<None>] Seq=1 Win=512 Len=2000 [TCP segment of a reassembled PDU]
    3 0.000161491     10.5.5.1 → 10.5.5.26    TCP 54 80 → 1388 [RST, ACK] Seq=1 Ack=2001 Win=0 Len=0
    4 4.790449682 PcsCompu_b1:8b:27 → PcsCompu_05:6d:d5 ARP 60 Who has 10.5.5.1? Tell 10.5.5.26
    5 4.790495643 PcsCompu_05:6d:d5 → PcsCompu_b1:8b:27 ARP 42 10.5.5.1 is at 08:00:27:05:6d:d5
    6 5.155094588 PcsCompu_05:6d:d5 → PcsCompu_b1:8b:27 ARP 42 Who has 10.5.5.26? Tell 10.5.5.1
    7 5.155921696 PcsCompu_b1:8b:27 → PcsCompu_05:6d:d5 ARP 60 10.5.5.26 is at 08:00:27:b1:8b:27

Packet 1 is the first fragment of the IP datagram, and packet 2 is the second fragment that contains the payload of the original IP datagram.

The "off=0" field in packet 1 indicates that this is the first fragment, and the "ID=000b" field is the identification number of the IP datagram.

The "TCP segment of a reassembled PDU" message in packet 2 indicates that this packet is part of a larger TCP segment that has been reassembled from the fragments.

The original IP datagram was fragmented into smaller packets to be transmitted over the network, and then reassembled at the destination host to reconstruct the original datagram.

[a@lan ~]$ ip a s dev enp0s9
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:a5:a2:be brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.27/27 brd 10.5.5.31 scope global noprefixroute dynamic enp0s9
       valid_lft 525sec preferred_lft 525sec
    inet6 fe80::6ba2:aa86:34f:ff09/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[a@lan ~]$ sudo hping3 -c 1 -d 5000 -F 10.5.5.1
No.     Time           Source                Destination           Protocol Length Info
     28 5.435211437    10.5.5.27             10.5.5.1              IPv4     1514   Fragmented IP protocol (proto=TCP 6, off=0, ID=00a7) [Reassembled in #31]

Frame 28: 1514 bytes on wire (12112 bits), 1514 bytes captured (12112 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 15, 2023 17:03:17.270058641 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692108197.270058641 seconds
    [Time delta from previous captured frame: 0.041511676 seconds]
    [Time delta from previous displayed frame: 0.000000000 seconds]
    [Time since reference or first frame: 5.435211437 seconds]
    Frame Number: 28
    Frame Length: 1514 bytes (12112 bits)
    Capture Length: 1514 bytes (12112 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:data]
Ethernet II, Src: CadmusCo_a5:a2:be (08:00:27:a5:a2:be), Dst: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
    Destination: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
        Address: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_a5:a2:be (08:00:27:a5:a2:be)
        Address: CadmusCo_a5:a2:be (08:00:27:a5:a2:be)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.5.5.27 (10.5.5.27), Dst: 10.5.5.1 (10.5.5.1)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 1500
    Identification: 0x00a7 (167)
    Flags: 0x01 (More Fragments)
        0... .... = Reserved bit: Not set
        .0.. .... = Don't fragment: Not set
        ..1. .... = More fragments: Set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x3650 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.5.5.27 (10.5.5.27)
    Destination: 10.5.5.1 (10.5.5.1)
    Reassembled IPv4 in frame: 31
Data (1480 bytes)
    Data: 053b0000494c19701b178c8d50010200addb000058585858...
    [Length: 1480]

0000  08 00 27 05 6d d5 08 00 27 a5 a2 be 08 00 45 00   ..'.m...'.....E.
0010  05 dc 00 a7 20 00 40 06 36 50 0a 05 05 1b 0a 05   .... .@.6P......
0020  05 01 05 3b 00 00 49 4c 19 70 1b 17 8c 8d 50 01   ...;..IL.p....P.
0030  02 00 ad db 00 00 58 58 58 58 58 58 58 58 58 58   ......XXXXXXXXXX
0040  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0050  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0060  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0070  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0080  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0090  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0100  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0110  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0120  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0130  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0140  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0150  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0160  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0170  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0180  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0190  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0200  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0210  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0220  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0230  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0240  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0250  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0260  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0270  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0280  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0290  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0300  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0310  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0320  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0330  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0340  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0350  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0360  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0370  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0380  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0390  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0400  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0410  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0420  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0430  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0440  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0450  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0460  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0470  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0480  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0490  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0500  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0510  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0520  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0530  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0540  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0550  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0560  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0570  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0580  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0590  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05e0  58 58 58 58 58 58 58 58 58 58                     XXXXXXXXXX
No.     Time           Source                Destination           Protocol Length Info
     29 5.435398424    10.5.5.27             10.5.5.1              IPv4     1514   Fragmented IP protocol (proto=TCP 6, off=1480, ID=00a7) [Reassembled in #31]

Frame 29: 1514 bytes on wire (12112 bits), 1514 bytes captured (12112 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 15, 2023 17:03:17.270245628 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692108197.270245628 seconds
    [Time delta from previous captured frame: 0.000186987 seconds]
    [Time delta from previous displayed frame: 0.000186987 seconds]
    [Time since reference or first frame: 5.435398424 seconds]
    Frame Number: 29
    Frame Length: 1514 bytes (12112 bits)
    Capture Length: 1514 bytes (12112 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:data]
Ethernet II, Src: CadmusCo_a5:a2:be (08:00:27:a5:a2:be), Dst: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
    Destination: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
        Address: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_a5:a2:be (08:00:27:a5:a2:be)
        Address: CadmusCo_a5:a2:be (08:00:27:a5:a2:be)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.5.5.27 (10.5.5.27), Dst: 10.5.5.1 (10.5.5.1)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 1500
    Identification: 0x00a7 (167)
    Flags: 0x01 (More Fragments)
        0... .... = Reserved bit: Not set
        .0.. .... = Don't fragment: Not set
        ..1. .... = More fragments: Set
    Fragment offset: 1480
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x3597 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.5.5.27 (10.5.5.27)
    Destination: 10.5.5.1 (10.5.5.1)
    Reassembled IPv4 in frame: 31
Data (1480 bytes)
    Data: 585858585858585858585858585858585858585858585858...
    [Length: 1480]

0000  08 00 27 05 6d d5 08 00 27 a5 a2 be 08 00 45 00   ..'.m...'.....E.
0010  05 dc 00 a7 20 b9 40 06 35 97 0a 05 05 1b 0a 05   .... .@.5.......
0020  05 01 58 58 58 58 58 58 58 58 58 58 58 58 58 58   ..XXXXXXXXXXXXXX
0030  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0040  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0050  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0060  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0070  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0080  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0090  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0100  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0110  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0120  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0130  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0140  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0150  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0160  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0170  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0180  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0190  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0200  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0210  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0220  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0230  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0240  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0250  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0260  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0270  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0280  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0290  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0300  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0310  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0320  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0330  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0340  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0350  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0360  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0370  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0380  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0390  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0400  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0410  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0420  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0430  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0440  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0450  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0460  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0470  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0480  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0490  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0500  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0510  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0520  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0530  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0540  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0550  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0560  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0570  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0580  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0590  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05e0  58 58 58 58 58 58 58 58 58 58                     XXXXXXXXXX
No.     Time           Source                Destination           Protocol Length Info
     30 5.435410764    10.5.5.27             10.5.5.1              IPv4     1514   Fragmented IP protocol (proto=TCP 6, off=2960, ID=00a7) [Reassembled in #31]

Frame 30: 1514 bytes on wire (12112 bits), 1514 bytes captured (12112 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 15, 2023 17:03:17.270257968 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692108197.270257968 seconds
    [Time delta from previous captured frame: 0.000012340 seconds]
    [Time delta from previous displayed frame: 0.000012340 seconds]
    [Time since reference or first frame: 5.435410764 seconds]
    Frame Number: 30
    Frame Length: 1514 bytes (12112 bits)
    Capture Length: 1514 bytes (12112 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:data]
Ethernet II, Src: CadmusCo_a5:a2:be (08:00:27:a5:a2:be), Dst: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
    Destination: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
        Address: CadmusCo_05:6d:d5 (08:00:27:05:6d:d5)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_a5:a2:be (08:00:27:a5:a2:be)
        Address: CadmusCo_a5:a2:be (08:00:27:a5:a2:be)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.5.5.27 (10.5.5.27), Dst: 10.5.5.1 (10.5.5.1)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 1500
    Identification: 0x00a7 (167)
    Flags: 0x01 (More Fragments)
        0... .... = Reserved bit: Not set
        .0.. .... = Don't fragment: Not set
        ..1. .... = More fragments: Set
    Fragment offset: 2960
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x34de [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.5.5.27 (10.5.5.27)
    Destination: 10.5.5.1 (10.5.5.1)
    Reassembled IPv4 in frame: 31
Data (1480 bytes)
    Data: 585858585858585858585858585858585858585858585858...
    [Length: 1480]

0000  08 00 27 05 6d d5 08 00 27 a5 a2 be 08 00 45 00   ..'.m...'.....E.
0010  05 dc 00 a7 21 72 40 06 34 de 0a 05 05 1b 0a 05   ....!r@.4.......
0020  05 01 58 58 58 58 58 58 58 58 58 58 58 58 58 58   ..XXXXXXXXXXXXXX
0030  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0040  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0050  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0060  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0070  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0080  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0090  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
00f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0100  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0110  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0120  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0130  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0140  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0150  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0160  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0170  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0180  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0190  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
01f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0200  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0210  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0220  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0230  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0240  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0250  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0260  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0270  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0280  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0290  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
02f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0300  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0310  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0320  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0330  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0340  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0350  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0360  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0370  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0380  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0390  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
03f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0400  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0410  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0420  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0430  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0440  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0450  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0460  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0470  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0480  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0490  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04e0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
04f0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0500  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0510  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0520  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0530  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0540  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0550  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0560  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0570  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0580  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
0590  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05a0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05b0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05c0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05d0  58 58 58 58 58 58 58 58 58 58 58 58 58 58 58 58   XXXXXXXXXXXXXXXX
05e0  58 58 58 58 58 58 58 58 58 58                     XXXXXXXXXX		

The three packet captures show a fragmented packet being sent from the source IP address 10.5.5.27 to the destination IP address 10.5.5.1. The original packet has a payload size of 4440 bytes, which is larger than the maximum transmission unit (MTU) of the network. As a result, the packet has been split into three fragments to be sent over the network.

The first fragment has a total length of 1500 bytes and is being sent using the TCP protocol. The "Flags" field in the IP header indicates that this is a fragmented packet, with the "More fragments" flag set to 1. The "Fragment offset" field is set to 0, indicating that this is the first fragment of the packet. The "Data" section of the packet contains 1480 bytes of data.

The second fragment has a total length of 1500 bytes and is being sent using the TCP protocol. The "Flags" field in the IP header indicates that this is a fragmented packet, with the "More fragments" flag set to 1. The "Fragment offset" field is set to 1480, indicating that this is the second fragment of the packet. The "Data" section of the packet contains 1480 bytes of data.

The third fragment has a total length of 1480 bytes and is being sent using the TCP protocol. The "Flags" field in the IP header indicates that this is a fragmented packet, with the "More fragments" flag set to 0. The "Fragment offset" field is set to 2960, indicating that this is the third and final fragment of the packet. The "Data" section of the packet contains 1480 bytes of data.

ubuntu@ip-172-31-42-130:~$ tshark -r captured_traffic3.pcapng -Y "tcp.flags.syn == 1 and t
cp.flags.ack == 1"
    2 0.000076319     10.5.5.1 → 10.5.5.26    TCP 74 5001 → 45177 [SYN, ACK] Seq=0 Ack=1 Win=65160 Len=0 MSS=1460 SACK_PERM TSval=1684596121 TSecr=1068021001 WS=128
	
The client (10.5.5.1) has sent a SYN packet to the server (10.5.5.26) with a source port of 5001 and a destination port of 45177. The server has responded with a SYN-ACK packet, indicating that it has received the SYN packet and is sending its own SYN packet to synchronize sequence numbers. The SYN-ACK packet has a sequence number of 0, an acknowledgement number of 1 (acknowledging the client's SYN packet), a window size of 65160 and with a payload length of 0, indicating that there is no data transfer taking place. The client will now send an ACK packet to acknowledge the server's SYN packet and complete the handshake.	

    3 0.000603912    10.5.5.26 → 10.5.5.1     TCP 66 45177 → 5001 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=1068021002 TSecr=1684596121	

Segment 3 is an acknowledgment of the initial SYN/ACK packet, confirming that the connection has been established.	

ubuntu@ip-172-31-42-130:~$ tshark -r captured_traffic3.pcapng -Y "tcp.flags.fin == 1 and tcp.flags.ack == 1"
   98 0.058638133    10.5.5.26 → 10.5.5.1     TCP 66 45177 → 5001 [FIN, ACK] Seq=1000001 Ack=1 Win=29312 Len=0 TSval=1068021060 TSecr=1684596164
   99 0.067338863     10.5.5.1 → 10.5.5.26    TCP 66 5001 → 45177 [FIN, ACK] Seq=1 Ack=1000002 Win=1703552 Len=0 TSval=1684596188 TSecr=1068021060
   
The output shows two TCP segments, the first of which has both the "FIN" and "ACK" flags set, indicating that the sender has finished sending data and is ready to close the connection, and that the receiver has acknowledged the receipt of the FIN packet and is also ready to close the connection. The second segment is an acknowledgment of the FIN packet.

Here's a breakdown of the fields in the TCP header of the two segments:

Segment 1:

    Source IP address: 10.5.5.26
    Destination IP address: 10.5.5.1
    Source port: 45177
    Destination port: 5001
    Sequence number: 1000001
    Acknowledgment number: 1
    Window size: 29312
    TCP flags: FIN, ACK
    TCP options: TSval=1068021060, TSecr=1684596164

The "FIN, ACK" flags indicate that this segment is a request to close the connection, and that the receiver has acknowledged the request. The "Seq" field is set to 1000001 to indicate that the sender has finished sending data up to byte 1000000. The "Ack" field is set to 1 to acknowledge the receipt of the last byte of data. The "Window size" field indicates the size of the receive window, which is the amount of data that the receiver can receive before sending an acknowledgment. The TCP options include the timestamp value (TSval) and timestamp echo reply (TSecr), which are used to measure the round-trip time of the connection.

Segment 2:

    Source IP address: 10.5.5.1
    Destination IP address: 10.5.5.26
    Source port: 5001
    Destination port: 45177
    Sequence number: 1
    Acknowledgment number: 1000002
    Window size: 1703552
    TCP flags: FIN, ACK
    TCP options: TSval=1684596188, TSecr=1068021060

The "FIN, ACK" flags indicate that this segment is an acknowledgment of the FIN packet, and that the receiver is also ready to close the connection. The "Seq" field is set to 1 to indicate that the sender has finished sending data up to byte 0. The "Ack" field is set to 1000002 to acknowledge the receipt of the FIN packet and to indicate that the receiver has finished receiving data up to byte 1000001. The "Window size" field indicates the size of the receive window, which is the amount of data that the sender can send before receiving an acknowledgment. The TCP options include the timestamp value (TSval) and timestamp echo reply (TSecr), which are used to measure the round-trip time of the connection.

In summary, the two TCP segments with the "FIN, ACK" flags set confirm that the sender and receiver have both agreed to close the connection, and that all data has been sent and received.   

    1 0.000000000    10.5.5.26 → 10.5.5.1     TCP 74 45177 → 5001 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM TSval=1068021001 TSecr=0 WS=128
    2 0.000076319     10.5.5.1 → 10.5.5.26    TCP 74 5001 → 45177 [SYN, ACK] Seq=0 Ack=1 Win=65160 Len=0 MSS=1460 SACK_PERM TSval=1684596121 TSecr=1068021001 WS=128
    3 0.000603912    10.5.5.26 → 10.5.5.1     TCP 66 45177 → 5001 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=1068021002 TSecr=1684596121

The MSS value is included in the TCP SYN packets exchanged during the three-way handshake. The MSS value is included in the TCP options field of the SYN packets.

In segment 1, the MSS value is included in the TCP options field of the SYN packet sent by the client with IP address 10.5.5.26:

    MSS=1460

This indicates that the client can handle TCP segments with a maximum size of 1460 bytes.

In segment 2, the MSS value is included in the TCP options field of the SYN/ACK packet sent by the server with IP address 10.5.5.1:

    MSS=1460

This indicates that the server can handle TCP segments with a maximum size of 1460 bytes.

[a@lan ~]$ ip a s enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:2b:02:de brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 302sec preferred_lft 302sec
    inet6 fe80::8c5b:d301:ecfa:ee42/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

No.     Time           Source                Destination           Protocol Length Info
   2057 30.388250037   10.0.2.15             192.178.25.174        TCP      54     58944 > https [RST] Seq=793 Win=0 Len=0

Frame 2057: 54 bytes on wire (432 bits), 54 bytes captured (432 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 19:30:13.337456777 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692549013.337456777 seconds
    [Time delta from previous captured frame: 0.000059131 seconds]
    [Time delta from previous displayed frame: 0.000000000 seconds]
    [Time since reference or first frame: 30.388250037 seconds]
    Frame Number: 2057
    Frame Length: 54 bytes (432 bits)
    Capture Length: 54 bytes (432 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp]
    [Coloring Rule Name: TCP RST]
    [Coloring Rule String: tcp.flags.reset eq 1]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: RealtekU_12:35:00 (52:54:00:12:35:00)
    Destination: RealtekU_12:35:00 (52:54:00:12:35:00)
        Address: RealtekU_12:35:00 (52:54:00:12:35:00)
        .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 192.178.25.174 (192.178.25.174)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 40
    Identification: 0xf297 (62103)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x61c9 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 192.178.25.174 (192.178.25.174)
Transmission Control Protocol, Src Port: 58944 (58944), Dst Port: https (443), Seq: 793, Len: 0
    Source port: 58944 (58944)
    Destination port: https (443)
    [Stream index: 14]
    Sequence number: 793    (relative sequence number)
    Header length: 20 bytes
    Flags: 0x004 (RST)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...0 .... = Acknowledgment: Not set
        .... .... 0... = Push: Not set
        .... .... .1.. = Reset: Set
            [Expert Info (Chat/Sequence): Connection reset (RST)]
                [Message: Connection reset (RST)]
                [Severity level: Chat]
                [Group: Sequence]
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 0
    [Calculated window size: 0]
    [Window size scaling factor: -2 (no window scaling used)]
    Checksum: 0xf4e6 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]

0000  52 54 00 12 35 00 08 00 27 2b 02 de 08 00 45 00   RT..5...'+....E.
0010  00 28 f2 97 40 00 40 06 61 c9 0a 00 02 0f c0 b2   .(..@.@.a.......
0020  19 ae e6 40 01 bb 13 3c d9 52 00 00 00 00 50 04   ...@...<.R....P.
0030  00 00 f4 e6 00 00                                 ......
No.     Time           Source                Destination           Protocol Length Info
   2059 30.388340142   10.0.2.15             192.178.25.174        TCP      54     58944 > https [RST] Seq=794 Win=0 Len=0

This is a TCP RST (reset) packet sent from the destination port (443) to the source port (58944). The RST flag is set, indicating that the connection is being reset. This packet is in response to the previous packet (frame 13) which was an HTTP GET request. The window size value is 0, indicating that the receiver's buffer is full and it cannot receive any more data. The checksum is valid. The SEQ/ACK analysis shows that this is an ACK to the previous segment and the RTT to ACK the segment was 0 seconds. The TCP Analysis Flags indicate that this is a suspected retransmission and the TCP checksum is incorrect.

[a@lan ~]$ iperf -c 8.8.8.8 -u -b 1M -l 64 -t 10 -p 53 -B 10.0.2.15
------------------------------------------------------------
Client connecting to 8.8.8.8, UDP port 53
Binding to local address 10.0.2.15
Sending 64 byte datagrams, IPG target: 488.28 us (kalman adjust)
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  3] local 10.0.2.15 port 45733 connected with 8.8.8.8 port 53
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-10.0 sec  1.25 MBytes  1.04 Mbits/sec
[  3] Sent 20441 datagrams

No.     Time           Source                Destination           Protocol Length Info
  10533 570.140426125  10.0.2.15             8.8.8.8               DNS      106    Standard query 0x0000  Unused <Root> Unused <Root>[Malformed Packet]

Frame 10533: 106 bytes on wire (848 bits), 106 bytes captured (848 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 20:42:37.556374976 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692553357.556374976 seconds
    [Time delta from previous captured frame: 0.000681481 seconds]
    [Time delta from previous displayed frame: 0.000681481 seconds]
    [Time since reference or first frame: 570.140426125 seconds]
    Frame Number: 10533
    Frame Length: 106 bytes (848 bits)
    Capture Length: 106 bytes (848 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:udp:dns]
    [Coloring Rule Name: UDP]
    [Coloring Rule String: udp]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: RealtekU_12:35:00 (52:54:00:12:35:00)
    Destination: RealtekU_12:35:00 (52:54:00:12:35:00)
        Address: RealtekU_12:35:00 (52:54:00:12:35:00)
        .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 8.8.8.8 (8.8.8.8)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 92
    Identification: 0xd5f9 (54777)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: UDP (17)
    Header checksum: 0x4879 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 8.8.8.8 (8.8.8.8)
User Datagram Protocol, Src Port: 45733 (45733), Dst Port: domain (53)
    Source port: 45733 (45733)
    Destination port: domain (53)
    Length: 72
    Checksum: 0x1c78 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
Domain Name System (query)
    Transaction ID: 0x0000
    Flags: 0x0006 Standard query
        0... .... .... .... = Response: Message is a query
        .000 0... .... .... = Opcode: Standard query (0)
        .... ..0. .... .... = Truncated: Message is not truncated
        .... ...0 .... .... = Recursion desired: Don't do query recursively
        .... .... .0.. .... = Z: reserved (0)
        .... .... ...0 .... = Non-authenticated data: Unacceptable
    Questions: 25826
    Answer RRs: 20621
    Authority RRs: 8
    Additional RRs: 32069
    Queries
        <Root>: type Unused, class Unknown (8)
            Name: <Root>
            Type: Unused (unused)
            Class: Unknown (0x0008)
        <Root>: type Unused, class Unknown (12337)
            Name: <Root>
            Type: Unused (unused)
            Class: Unknown (0x3031)
[Malformed Packet: DNS]
    [Expert Info (Error/Malformed): Malformed Packet (Exception occurred)]
        [Message: Malformed Packet (Exception occurred)]
        [Severity level: Error]
        [Group: Malformed]

0000  52 54 00 12 35 00 08 00 27 2b 02 de 08 00 45 00   RT..5...'+....E.
0010  00 5c d5 f9 40 00 40 11 48 79 0a 00 02 0f 08 08   .\..@.@.Hy......
0020  08 08 b2 a5 00 35 00 48 1c 78 00 00 00 06 64 e2   .....5.H.x....d.
0030  50 8d 00 08 7d 45 00 00 00 00 08 00 00 00 30 31   P...}E........01
0040  32 33 34 35 36 37 38 39 30 31 32 33 34 35 36 37   2345678901234567
0050  38 39 30 31 00 34 34 35 36 37 38 39 30 31 32 33   8901.44567890123
0060  34 35 36 37 38 39 30 31 32 33                     4567890123

No.     Time           Source                Destination           Protocol Length Info
      4 18.506264948   10.0.2.15             10.0.2.3              DHCP     342    DHCP Request  - Transaction ID 0x95021c4e

Frame 4: 342 bytes on wire (2736 bits), 342 bytes captured (2736 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 21:01:30.393210868 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692554490.393210868 seconds
    [Time delta from previous captured frame: 0.000023571 seconds]
    [Time delta from previous displayed frame: 0.000000000 seconds]
    [Time since reference or first frame: 18.506264948 seconds]
    Frame Number: 4
    Frame Length: 342 bytes (2736 bits)
    Capture Length: 342 bytes (2736 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:udp:bootp]
    [Coloring Rule Name: UDP]
    [Coloring Rule String: udp]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
    Destination: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        Address: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 10.0.2.3 (10.0.2.3)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 328
    Identification: 0x8b1a (35610)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: UDP (17)
    Header checksum: 0x9679 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 10.0.2.3 (10.0.2.3)
User Datagram Protocol, Src Port: bootpc (68), Dst Port: bootps (67)
    Source port: bootpc (68)
    Destination port: bootps (67)
    Length: 308
    Checksum: 0x1957 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
Bootstrap Protocol
    Message type: Boot Request (1)
    Hardware type: Ethernet (0x01)
    Hardware address length: 6
    Hops: 0
    Transaction ID: 0x95021c4e
    Seconds elapsed: 0
    Bootp flags: 0x0000 (Unicast)
        0... .... .... .... = Broadcast flag: Unicast
        .000 0000 0000 0000 = Reserved flags: 0x0000
    Client IP address: 10.0.2.15 (10.0.2.15)
    Your (client) IP address: 0.0.0.0 (0.0.0.0)
    Next server IP address: 0.0.0.0 (0.0.0.0)
    Relay agent IP address: 0.0.0.0 (0.0.0.0)
    Client MAC address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Client hardware address padding: 00000000000000000000
    Server host name not given
    Boot file name not given
    Magic cookie: DHCP
    Option: (53) DHCP Message Type
        Length: 1
        DHCP: Request (3)
    Option: (12) Host Name
        Length: 3
        Host Name: lan
    Option: (55) Parameter Request List
        Length: 19
        Parameter Request List Item: (1) Subnet Mask
        Parameter Request List Item: (28) Broadcast Address
        Parameter Request List Item: (2) Time Offset
        Parameter Request List Item: (121) Classless Static Route
        Parameter Request List Item: (15) Domain Name
        Parameter Request List Item: (6) Domain Name Server
        Parameter Request List Item: (12) Host Name
        Parameter Request List Item: (40) Network Information Service Domain
        Parameter Request List Item: (41) Network Information Service Servers
        Parameter Request List Item: (42) Network Time Protocol Servers
        Parameter Request List Item: (26) Interface MTU
        Parameter Request List Item: (119) Domain Search
        Parameter Request List Item: (3) Router
        Parameter Request List Item: (121) Classless Static Route
        Parameter Request List Item: (249) Private/Classless Static Route (Microsoft)
        Parameter Request List Item: (33) Static Route
        Parameter Request List Item: (252) Private/Proxy autodiscovery
        Parameter Request List Item: (42) Network Time Protocol Servers
        Parameter Request List Item: (17) Root Path
    Option: (255) End
        Option End: 255
    Padding

0000  08 00 27 d8 a3 97 08 00 27 2b 02 de 08 00 45 00   ..'.....'+....E.
0010  01 48 8b 1a 40 00 40 11 96 79 0a 00 02 0f 0a 00   .H..@.@..y......
0020  02 03 00 44 00 43 01 34 19 57 01 01 06 00 95 02   ...D.C.4.W......
0030  1c 4e 00 00 00 00 0a 00 02 0f 00 00 00 00 00 00   .N..............
0040  00 00 00 00 00 00 08 00 27 2b 02 de 00 00 00 00   ........'+......
0050  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0060  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0070  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0080  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0090  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00a0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00b0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00d0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00e0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00f0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0100  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0110  00 00 00 00 00 00 63 82 53 63 35 01 03 0c 03 6c   ......c.Sc5....l
0120  61 6e 37 13 01 1c 02 79 0f 06 0c 28 29 2a 1a 77   an7....y...()*.w
0130  03 79 f9 21 fc 2a 11 ff 00 00 00 00 00 00 00 00   .y.!.*..........
0140  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0150  00 00 00 00 00 00                                 ......
No.     Time           Source                Destination           Protocol Length Info
      5 18.562848482   10.0.2.3              10.0.2.15             DHCP     590    DHCP ACK      - Transaction ID 0x95021c4e

Frame 5: 590 bytes on wire (4720 bits), 590 bytes captured (4720 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 21:01:30.449794402 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692554490.449794402 seconds
    [Time delta from previous captured frame: 0.056583534 seconds]
    [Time delta from previous displayed frame: 0.056583534 seconds]
    [Time since reference or first frame: 18.562848482 seconds]
    Frame Number: 5
    Frame Length: 590 bytes (4720 bits)
    Capture Length: 590 bytes (4720 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:udp:bootp]
    [Coloring Rule Name: UDP]
    [Coloring Rule String: udp]
Ethernet II, Src: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97), Dst: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Destination: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        Address: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.3 (10.0.2.3), Dst: 10.0.2.15 (10.0.2.15)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 576
    Identification: 0x001a (26)
    Flags: 0x00
        0... .... = Reserved bit: Not set
        .0.. .... = Don't fragment: Not set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 255
    Protocol: UDP (17)
    Header checksum: 0xa181 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.3 (10.0.2.3)
    Destination: 10.0.2.15 (10.0.2.15)
User Datagram Protocol, Src Port: bootps (67), Dst Port: bootpc (68)
    Source port: bootps (67)
    Destination port: bootpc (68)
    Length: 556
    Checksum: 0x978c [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
Bootstrap Protocol
    Message type: Boot Reply (2)
    Hardware type: Ethernet (0x01)
    Hardware address length: 6
    Hops: 0
    Transaction ID: 0x95021c4e
    Seconds elapsed: 0
    Bootp flags: 0x0000 (Unicast)
        0... .... .... .... = Broadcast flag: Unicast
        .000 0000 0000 0000 = Reserved flags: 0x0000
    Client IP address: 10.0.2.15 (10.0.2.15)
    Your (client) IP address: 10.0.2.15 (10.0.2.15)
    Next server IP address: 0.0.0.0 (0.0.0.0)
    Relay agent IP address: 0.0.0.0 (0.0.0.0)
    Client MAC address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Client hardware address padding: 00000000000000000000
    Server host name not given
    Boot file name not given
    Magic cookie: DHCP
    Option: (54) DHCP Server Identifier
        Length: 4
        DHCP Server Identifier: 10.0.2.3 (10.0.2.3)
    Option: (53) DHCP Message Type
        Length: 1
        DHCP: ACK (5)
    Option: (1) Subnet Mask
        Length: 4
        Subnet Mask: 255.255.255.0 (255.255.255.0)
    Option: (3) Router
        Length: 4
        Router: 10.0.2.1 (10.0.2.1)
    Option: (6) Domain Name Server
        Length: 4
        Domain Name Server: 192.168.0.1 (192.168.0.1)
    Option: (51) IP Address Lease Time
        Length: 4
        IP Address Lease Time: (600s) 10 minutes
    Option: (255) End
        Option End: 255
    Padding

0000  08 00 27 2b 02 de 08 00 27 d8 a3 97 08 00 45 00   ..'+....'.....E.
0010  02 40 00 1a 00 00 ff 11 a1 81 0a 00 02 03 0a 00   .@..............
0020  02 0f 00 43 00 44 02 2c 97 8c 02 01 06 00 95 02   ...C.D.,........
0030  1c 4e 00 00 00 00 0a 00 02 0f 0a 00 02 0f 00 00   .N..............
0040  00 00 00 00 00 00 08 00 27 2b 02 de 00 00 00 00   ........'+......
0050  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0060  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0070  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0080  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0090  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00a0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00b0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00d0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00e0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00f0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0100  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0110  00 00 00 00 00 00 63 82 53 63 36 04 0a 00 02 03   ......c.Sc6.....
0120  35 01 05 01 04 ff ff ff 00 03 04 0a 00 02 01 06   5...............
0130  04 c0 a8 00 01 33 04 00 00 02 58 ff 00 00 00 00   .....3....X.....
0140  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0150  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0160  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0170  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0180  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0190  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01a0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01b0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01d0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01e0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01f0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0200  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0210  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0220  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0230  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0240  00 00 00 00 00 00 00 00 00 00 00 00 00 00         ..............
No.     Time           Source                Destination           Protocol Length Info
     14 213.285105156  10.0.2.15             10.0.2.3              DHCP     342    DHCP Request  - Transaction ID 0x388aa677

Frame 14: 342 bytes on wire (2736 bits), 342 bytes captured (2736 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 21:04:45.172051076 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692554685.172051076 seconds
    [Time delta from previous captured frame: 117.280109053 seconds]
    [Time delta from previous displayed frame: 194.722256674 seconds]
    [Time since reference or first frame: 213.285105156 seconds]
    Frame Number: 14
    Frame Length: 342 bytes (2736 bits)
    Capture Length: 342 bytes (2736 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:udp:bootp]
    [Coloring Rule Name: UDP]
    [Coloring Rule String: udp]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
    Destination: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        Address: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 10.0.2.3 (10.0.2.3)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 328
    Identification: 0xef22 (61218)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: UDP (17)
    Header checksum: 0x3271 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 10.0.2.3 (10.0.2.3)
User Datagram Protocol, Src Port: bootpc (68), Dst Port: bootps (67)
    Source port: bootpc (68)
    Destination port: bootps (67)
    Length: 308
    Checksum: 0x1957 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
Bootstrap Protocol
    Message type: Boot Request (1)
    Hardware type: Ethernet (0x01)
    Hardware address length: 6
    Hops: 0
    Transaction ID: 0x388aa677
    Seconds elapsed: 0
    Bootp flags: 0x0000 (Unicast)
        0... .... .... .... = Broadcast flag: Unicast
        .000 0000 0000 0000 = Reserved flags: 0x0000
    Client IP address: 10.0.2.15 (10.0.2.15)
    Your (client) IP address: 0.0.0.0 (0.0.0.0)
    Next server IP address: 0.0.0.0 (0.0.0.0)
    Relay agent IP address: 0.0.0.0 (0.0.0.0)
    Client MAC address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Client hardware address padding: 00000000000000000000
    Server host name not given
    Boot file name not given
    Magic cookie: DHCP
    Option: (53) DHCP Message Type
        Length: 1
        DHCP: Request (3)
    Option: (55) Parameter Request List
        Length: 13
        Parameter Request List Item: (1) Subnet Mask
        Parameter Request List Item: (28) Broadcast Address
        Parameter Request List Item: (2) Time Offset
        Parameter Request List Item: (121) Classless Static Route
        Parameter Request List Item: (15) Domain Name
        Parameter Request List Item: (6) Domain Name Server
        Parameter Request List Item: (12) Host Name
        Parameter Request List Item: (40) Network Information Service Domain
        Parameter Request List Item: (41) Network Information Service Servers
        Parameter Request List Item: (42) Network Time Protocol Servers
        Parameter Request List Item: (26) Interface MTU
        Parameter Request List Item: (119) Domain Search
        Parameter Request List Item: (3) Router
    Option: (255) End
        Option End: 255
    Padding

0000  08 00 27 d8 a3 97 08 00 27 2b 02 de 08 00 45 00   ..'.....'+....E.
0010  01 48 ef 22 40 00 40 11 32 71 0a 00 02 0f 0a 00   .H."@.@.2q......
0020  02 03 00 44 00 43 01 34 19 57 01 01 06 00 38 8a   ...D.C.4.W....8.
0030  a6 77 00 00 00 00 0a 00 02 0f 00 00 00 00 00 00   .w..............
0040  00 00 00 00 00 00 08 00 27 2b 02 de 00 00 00 00   ........'+......
0050  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0060  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0070  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0080  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0090  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00a0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00b0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00d0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00e0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00f0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0100  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0110  00 00 00 00 00 00 63 82 53 63 35 01 03 37 0d 01   ......c.Sc5..7..
0120  1c 02 79 0f 06 0c 28 29 2a 1a 77 03 ff 00 00 00   ..y...()*.w.....
0130  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0140  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0150  00 00 00 00 00 00                                 ......
No.     Time           Source                Destination           Protocol Length Info
     15 213.342716290  10.0.2.3              10.0.2.15             DHCP     590    DHCP ACK      - Transaction ID 0x388aa677

Frame 15: 590 bytes on wire (4720 bits), 590 bytes captured (4720 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 21:04:45.229662210 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692554685.229662210 seconds
    [Time delta from previous captured frame: 0.057611134 seconds]
    [Time delta from previous displayed frame: 0.057611134 seconds]
    [Time since reference or first frame: 213.342716290 seconds]
    Frame Number: 15
    Frame Length: 590 bytes (4720 bits)
    Capture Length: 590 bytes (4720 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:udp:bootp]
    [Coloring Rule Name: UDP]
    [Coloring Rule String: udp]
Ethernet II, Src: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97), Dst: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Destination: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        Address: CadmusCo_d8:a3:97 (08:00:27:d8:a3:97)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.3 (10.0.2.3), Dst: 10.0.2.15 (10.0.2.15)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 576
    Identification: 0x001b (27)
    Flags: 0x00
        0... .... = Reserved bit: Not set
        .0.. .... = Don't fragment: Not set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 255
    Protocol: UDP (17)
    Header checksum: 0xa180 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.3 (10.0.2.3)
    Destination: 10.0.2.15 (10.0.2.15)
User Datagram Protocol, Src Port: bootps (67), Dst Port: bootpc (68)
    Source port: bootps (67)
    Destination port: bootpc (68)
    Length: 556
    Checksum: 0x69db [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
Bootstrap Protocol
    Message type: Boot Reply (2)
    Hardware type: Ethernet (0x01)
    Hardware address length: 6
    Hops: 0
    Transaction ID: 0x388aa677
    Seconds elapsed: 0
    Bootp flags: 0x0000 (Unicast)
        0... .... .... .... = Broadcast flag: Unicast
        .000 0000 0000 0000 = Reserved flags: 0x0000
    Client IP address: 10.0.2.15 (10.0.2.15)
    Your (client) IP address: 10.0.2.15 (10.0.2.15)
    Next server IP address: 0.0.0.0 (0.0.0.0)
    Relay agent IP address: 0.0.0.0 (0.0.0.0)
    Client MAC address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Client hardware address padding: 00000000000000000000
    Server host name not given
    Boot file name not given
    Magic cookie: DHCP
    Option: (54) DHCP Server Identifier
        Length: 4
        DHCP Server Identifier: 10.0.2.3 (10.0.2.3)
    Option: (53) DHCP Message Type
        Length: 1
        DHCP: ACK (5)
    Option: (1) Subnet Mask
        Length: 4
        Subnet Mask: 255.255.255.0 (255.255.255.0)
    Option: (3) Router
        Length: 4
        Router: 10.0.2.1 (10.0.2.1)
    Option: (6) Domain Name Server
        Length: 4
        Domain Name Server: 192.168.0.1 (192.168.0.1)
    Option: (51) IP Address Lease Time
        Length: 4
        IP Address Lease Time: (600s) 10 minutes
    Option: (255) End
        Option End: 255
    Padding

0000  08 00 27 2b 02 de 08 00 27 d8 a3 97 08 00 45 00   ..'+....'.....E.
0010  02 40 00 1b 00 00 ff 11 a1 80 0a 00 02 03 0a 00   .@..............
0020  02 0f 00 43 00 44 02 2c 69 db 02 01 06 00 38 8a   ...C.D.,i.....8.
0030  a6 77 00 00 00 00 0a 00 02 0f 0a 00 02 0f 00 00   .w..............
0040  00 00 00 00 00 00 08 00 27 2b 02 de 00 00 00 00   ........'+......
0050  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0060  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0070  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0080  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0090  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00a0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00b0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00d0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00e0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
00f0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0100  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0110  00 00 00 00 00 00 63 82 53 63 36 04 0a 00 02 03   ......c.Sc6.....
0120  35 01 05 01 04 ff ff ff 00 03 04 0a 00 02 01 06   5...............
0130  04 c0 a8 00 01 33 04 00 00 02 58 ff 00 00 00 00   .....3....X.....
0140  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0150  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0160  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0170  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0180  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0190  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01a0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01b0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01d0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01e0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
01f0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0200  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0210  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0220  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0230  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0240  00 00 00 00 00 00 00 00 00 00 00 00 00 00         ..............

No.     Time           Source                Destination           Protocol Length Info
      3 0.003165425    10.0.2.15             162.159.61.4          TCP      54     54684 > https [ACK] Seq=40 Ack=40 Win=64240 Len=0

Frame 3: 54 bytes on wire (432 bits), 54 bytes captured (432 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 21:35:50.261864872 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692556550.261864872 seconds
    [Time delta from previous captured frame: 0.000056699 seconds]
    [Time delta from previous displayed frame: 0.000056699 seconds]
    [Time since reference or first frame: 0.003165425 seconds]
    Frame Number: 3
    Frame Length: 54 bytes (432 bits)
    Capture Length: 54 bytes (432 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp]
    [Coloring Rule Name: TCP]
    [Coloring Rule String: tcp]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: RealtekU_12:35:00 (52:54:00:12:35:00)
    Destination: RealtekU_12:35:00 (52:54:00:12:35:00)
        Address: RealtekU_12:35:00 (52:54:00:12:35:00)
        .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 162.159.61.4 (162.159.61.4)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 40
    Identification: 0x347e (13438)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x1aa0 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 162.159.61.4 (162.159.61.4)
Transmission Control Protocol, Src Port: 54684 (54684), Dst Port: https (443), Seq: 40, Ack: 40, Len: 0
    Source port: 54684 (54684)
    Destination port: https (443)
    [Stream index: 0]
    Sequence number: 40    (relative sequence number)
    Acknowledgment number: 40    (relative ack number)
    Header length: 20 bytes
    Flags: 0x010 (ACK)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...1 .... = Acknowledgment: Set
        .... .... 0... = Push: Not set
        .... .... .0.. = Reset: Not set
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 64240
    [Calculated window size: 64240]
    [Window size scaling factor: -1 (unknown)]
    Checksum: 0xebcc [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
    [SEQ/ACK analysis]
        [This is an ACK to the segment in frame: 2]
        [The RTT to ACK the segment was: 0.000056699 seconds]

0000  52 54 00 12 35 00 08 00 27 2b 02 de 08 00 45 00   RT..5...'+....E.
0010  00 28 34 7e 40 00 40 06 1a a0 0a 00 02 0f a2 9f   .(4~@.@.........
0020  3d 04 d5 9c 01 bb 77 50 97 3f 00 07 74 59 50 10   =.....wP.?..tYP.
0030  fa f0 eb cc 00 00                                 ......

No.     Time           Source                Destination           Protocol Length Info
      1 0.000000000    10.0.2.15             162.159.61.4          TLSv1.2  93     Application Data

Frame 1: 93 bytes on wire (744 bits), 93 bytes captured (744 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 21:35:50.258699447 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692556550.258699447 seconds
    [Time delta from previous captured frame: 0.000000000 seconds]
    [Time delta from previous displayed frame: 0.000000000 seconds]
    [Time since reference or first frame: 0.000000000 seconds]
    Frame Number: 1
    Frame Length: 93 bytes (744 bits)
    Capture Length: 93 bytes (744 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp:ssl]
    [Coloring Rule Name: TCP]
    [Coloring Rule String: tcp]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: RealtekU_12:35:00 (52:54:00:12:35:00)
    Destination: RealtekU_12:35:00 (52:54:00:12:35:00)
        Address: RealtekU_12:35:00 (52:54:00:12:35:00)
        .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 162.159.61.4 (162.159.61.4)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 79
    Identification: 0x347d (13437)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x1a7a [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 162.159.61.4 (162.159.61.4)
Transmission Control Protocol, Src Port: 54684 (54684), Dst Port: https (443), Seq: 1, Ack: 1, Len: 39
    Source port: 54684 (54684)
    Destination port: https (443)
    [Stream index: 0]
    Sequence number: 1    (relative sequence number)
    [Next sequence number: 40    (relative sequence number)]
    Acknowledgment number: 1    (relative ack number)
    Header length: 20 bytes
    Flags: 0x018 (PSH, ACK)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...1 .... = Acknowledgment: Set
        .... .... 1... = Push: Set
        .... .... .0.. = Reset: Not set
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 64240
    [Calculated window size: 64240]
    [Window size scaling factor: -1 (unknown)]
    Checksum: 0xebf3 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
    [SEQ/ACK analysis]
        [Bytes in flight: 39]
Secure Sockets Layer
    TLSv1.2 Record Layer: Application Data Protocol: http
        Content Type: Application Data (23)
        Version: TLS 1.2 (0x0303)
        Length: 34
        Encrypted Application Data: 436138c9306944a24c7bf33b7417fad9c48cc5e8bfcf7916...

0000  52 54 00 12 35 00 08 00 27 2b 02 de 08 00 45 00   RT..5...'+....E.
0010  00 4f 34 7d 40 00 40 06 1a 7a 0a 00 02 0f a2 9f   .O4}@.@..z......
0020  3d 04 d5 9c 01 bb 77 50 97 18 00 07 74 32 50 18   =.....wP....t2P.
0030  fa f0 eb f3 00 00 17 03 03 00 22 43 61 38 c9 30   .........."Ca8.0
0040  69 44 a2 4c 7b f3 3b 74 17 fa d9 c4 8c c5 e8 bf   iD.L{.;t........
0050  cf 79 16 32 cc 0c c4 f7 dd 68 ad 30 36            .y.2.....h.06
[a@lan ~]$ cat 443*
No.     Time           Source                Destination           Protocol Length Info
      1 0.000000000    10.0.2.15             162.159.61.4          TLSv1.2  93     Application Data

Frame 1: 93 bytes on wire (744 bits), 93 bytes captured (744 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 21:35:50.258699447 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692556550.258699447 seconds
    [Time delta from previous captured frame: 0.000000000 seconds]
    [Time delta from previous displayed frame: 0.000000000 seconds]
    [Time since reference or first frame: 0.000000000 seconds]
    Frame Number: 1
    Frame Length: 93 bytes (744 bits)
    Capture Length: 93 bytes (744 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp:ssl]
    [Coloring Rule Name: TCP]
    [Coloring Rule String: tcp]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: RealtekU_12:35:00 (52:54:00:12:35:00)
    Destination: RealtekU_12:35:00 (52:54:00:12:35:00)
        Address: RealtekU_12:35:00 (52:54:00:12:35:00)
        .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 162.159.61.4 (162.159.61.4)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 79
    Identification: 0x347d (13437)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x1a7a [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 162.159.61.4 (162.159.61.4)
Transmission Control Protocol, Src Port: 54684 (54684), Dst Port: https (443), Seq: 1, Ack: 1, Len: 39
    Source port: 54684 (54684)
    Destination port: https (443)
    [Stream index: 0]
    Sequence number: 1    (relative sequence number)
    [Next sequence number: 40    (relative sequence number)]
    Acknowledgment number: 1    (relative ack number)
    Header length: 20 bytes
    Flags: 0x018 (PSH, ACK)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...1 .... = Acknowledgment: Set
        .... .... 1... = Push: Set
        .... .... .0.. = Reset: Not set
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 64240
    [Calculated window size: 64240]
    [Window size scaling factor: -1 (unknown)]
    Checksum: 0xebf3 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
    [SEQ/ACK analysis]
        [Bytes in flight: 39]
Secure Sockets Layer
    TLSv1.2 Record Layer: Application Data Protocol: http
        Content Type: Application Data (23)
        Version: TLS 1.2 (0x0303)
        Length: 34
        Encrypted Application Data: 436138c9306944a24c7bf33b7417fad9c48cc5e8bfcf7916...

0000  52 54 00 12 35 00 08 00 27 2b 02 de 08 00 45 00   RT..5...'+....E.
0010  00 4f 34 7d 40 00 40 06 1a 7a 0a 00 02 0f a2 9f   .O4}@.@..z......
0020  3d 04 d5 9c 01 bb 77 50 97 18 00 07 74 32 50 18   =.....wP....t2P.
0030  fa f0 eb f3 00 00 17 03 03 00 22 43 61 38 c9 30   .........."Ca8.0
0040  69 44 a2 4c 7b f3 3b 74 17 fa d9 c4 8c c5 e8 bf   iD.L{.;t........
0050  cf 79 16 32 cc 0c c4 f7 dd 68 ad 30 36            .y.2.....h.06

    3. Search for logins and passwords in HTTP and FTP traffic.

Frame 3: 66 bytes on wire (528 bits), 66 bytes captured (528 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 23:43:07.736401540 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692564187.736401540 seconds
    [Time delta from previous captured frame: 0.000551992 seconds]
    [Time delta from previous displayed frame: 0.000551992 seconds]
    [Time since reference or first frame: 0.000654457 seconds]
    Frame Number: 3
    Frame Length: 66 bytes (528 bits)
    Capture Length: 66 bytes (528 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp]
    [Coloring Rule Name: HTTP]
    [Coloring Rule String: http || tcp.port == 80]
Ethernet II, Src: CadmusCo_41:52:ce (08:00:27:41:52:ce), Dst: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Destination: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        Address: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.13 (10.0.2.13), Dst: 10.0.2.15 (10.0.2.15)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 52
    Identification: 0x93ca (37834)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x8ede [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.13 (10.0.2.13)
    Destination: 10.0.2.15 (10.0.2.15)
Transmission Control Protocol, Src Port: 35146 (35146), Dst Port: http (80), Seq: 1, Ack: 1, Len: 0
    Source port: 35146 (35146)
    Destination port: http (80)
    [Stream index: 0]
    Sequence number: 1    (relative sequence number)
    Acknowledgment number: 1    (relative ack number)
    Header length: 32 bytes
    Flags: 0x010 (ACK)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...1 .... = Acknowledgment: Set
        .... .... 0... = Push: Not set
        .... .... .0.. = Reset: Not set
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 502
    [Calculated window size: 64256]
    [Window size scaling factor: 128]
    Checksum: 0x9690 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
    Options: (12 bytes), No-Operation (NOP), No-Operation (NOP), Timestamps
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        Timestamps: TSval 3771086418, TSecr 15432206
            Kind: Timestamp (8)
            Length: 10
            Timestamp value: 3771086418
            Timestamp echo reply: 15432206
    [SEQ/ACK analysis]
        [This is an ACK to the segment in frame: 2]
        [The RTT to ACK the segment was: 0.000551992 seconds]

0000  08 00 27 2b 02 de 08 00 27 41 52 ce 08 00 45 00   ..'+....'AR...E.
0010  00 34 93 ca 40 00 40 06 8e de 0a 00 02 0d 0a 00   .4..@.@.........
0020  02 0f 89 4a 00 50 d7 19 3e 3c ae 72 e6 a5 80 10   ...J.P..><.r....
0030  01 f6 96 90 00 00 01 01 08 0a e0 c6 36 52 00 eb   ............6R..
0040  7a 0e                                             z.
No.     Time           Source                Destination           Protocol Length Info
      4 0.000811768    10.0.2.13             10.0.2.15             HTTP     169    GET /1.php?user=test&password=12345 HTTP/1.1

Frame 4: 169 bytes on wire (1352 bits), 169 bytes captured (1352 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 23:43:07.736558851 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692564187.736558851 seconds
    [Time delta from previous captured frame: 0.000157311 seconds]
    [Time delta from previous displayed frame: 0.000157311 seconds]
    [Time since reference or first frame: 0.000811768 seconds]
    Frame Number: 4
    Frame Length: 169 bytes (1352 bits)
    Capture Length: 169 bytes (1352 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp:http]
    [Number of per-protocol-data: 1]
    [Hypertext Transfer Protocol, key 0]
    [Coloring Rule Name: HTTP]
    [Coloring Rule String: http || tcp.port == 80]
Ethernet II, Src: CadmusCo_41:52:ce (08:00:27:41:52:ce), Dst: CadmusCo_2b:02:de (08:00:27:2b:02:de)
    Destination: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        Address: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.13 (10.0.2.13), Dst: 10.0.2.15 (10.0.2.15)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 155
    Identification: 0x93cb (37835)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x8e76 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.13 (10.0.2.13)
    Destination: 10.0.2.15 (10.0.2.15)
Transmission Control Protocol, Src Port: 35146 (35146), Dst Port: http (80), Seq: 1, Ack: 1, Len: 103
    Source port: 35146 (35146)
    Destination port: http (80)
    [Stream index: 0]
    Sequence number: 1    (relative sequence number)
    [Next sequence number: 104    (relative sequence number)]
    Acknowledgment number: 1    (relative ack number)
    Header length: 32 bytes
    Flags: 0x018 (PSH, ACK)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...1 .... = Acknowledgment: Set
        .... .... 1... = Push: Set
        .... .... .0.. = Reset: Not set
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 502
    [Calculated window size: 64256]
    [Window size scaling factor: 128]
    Checksum: 0x6203 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
    Options: (12 bytes), No-Operation (NOP), No-Operation (NOP), Timestamps
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        Timestamps: TSval 3771086418, TSecr 15432206
            Kind: Timestamp (8)
            Length: 10
            Timestamp value: 3771086418
            Timestamp echo reply: 15432206
    [SEQ/ACK analysis]
        [Bytes in flight: 103]
Hypertext Transfer Protocol
    GET /1.php?user=test&password=12345 HTTP/1.1\r\n
        [Expert Info (Chat/Sequence): GET /1.php?user=test&password=12345 HTTP/1.1\r\n]
            [Message: GET /1.php?user=test&password=12345 HTTP/1.1\r\n]
            [Severity level: Chat]
            [Group: Sequence]
        Request Method: GET
        Request URI: /1.php?user=test&password=12345
        Request Version: HTTP/1.1
    Host: 10.0.2.15\r\n
    User-Agent: curl/7.81.0\r\n
    Accept: */*\r\n
    \r\n
    [Full request URI: http://10.0.2.15/1.php?user=test&password=12345]
    [HTTP request 1/1]
    [Response in frame: 6]

0000  08 00 27 2b 02 de 08 00 27 41 52 ce 08 00 45 00   ..'+....'AR...E.
0010  00 9b 93 cb 40 00 40 06 8e 76 0a 00 02 0d 0a 00   ....@.@..v......
0020  02 0f 89 4a 00 50 d7 19 3e 3c ae 72 e6 a5 80 18   ...J.P..><.r....
0030  01 f6 62 03 00 00 01 01 08 0a e0 c6 36 52 00 eb   ..b.........6R..
0040  7a 0e 47 45 54 20 2f 31 2e 70 68 70 3f 75 73 65   z.GET /1.php?use
0050  72 3d 74 65 73 74 26 70 61 73 73 77 6f 72 64 3d   r=test&password=
0060  31 32 33 34 35 20 48 54 54 50 2f 31 2e 31 0d 0a   12345 HTTP/1.1..
0070  48 6f 73 74 3a 20 31 30 2e 30 2e 32 2e 31 35 0d   Host: 10.0.2.15.
0080  0a 55 73 65 72 2d 41 67 65 6e 74 3a 20 63 75 72   .User-Agent: cur
0090  6c 2f 37 2e 38 31 2e 30 0d 0a 41 63 63 65 70 74   l/7.81.0..Accept
00a0  3a 20 2a 2f 2a 0d 0a 0d 0a                        : */*....
No.     Time           Source                Destination           Protocol Length Info
      5 0.000855911    10.0.2.15             10.0.2.13             TCP      66     http > 35146 [ACK] Seq=1 Ack=104 Win=29056 Len=0 TSval=15432207 TSecr=3771086418

Frame 5: 66 bytes on wire (528 bits), 66 bytes captured (528 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 23:43:07.736602994 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692564187.736602994 seconds
    [Time delta from previous captured frame: 0.000044143 seconds]
    [Time delta from previous displayed frame: 0.000044143 seconds]
    [Time since reference or first frame: 0.000855911 seconds]
    Frame Number: 5
    Frame Length: 66 bytes (528 bits)
    Capture Length: 66 bytes (528 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp]
    [Coloring Rule Name: HTTP]
    [Coloring Rule String: http || tcp.port == 80]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: CadmusCo_41:52:ce (08:00:27:41:52:ce)
    Destination: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        Address: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 10.0.2.13 (10.0.2.13)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 52
    Identification: 0x9caa (40106)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x85fe [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 10.0.2.13 (10.0.2.13)
Transmission Control Protocol, Src Port: http (80), Dst Port: 35146 (35146), Seq: 1, Ack: 104, Len: 0
    Source port: http (80)
    Destination port: 35146 (35146)
    [Stream index: 0]
    Sequence number: 1    (relative sequence number)
    Acknowledgment number: 104    (relative ack number)
    Header length: 32 bytes
    Flags: 0x010 (ACK)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...1 .... = Acknowledgment: Set
        .... .... 0... = Push: Not set
        .... .... .0.. = Reset: Not set
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 227
    [Calculated window size: 29056]
    [Window size scaling factor: 128]
    Checksum: 0x1842 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
    Options: (12 bytes), No-Operation (NOP), No-Operation (NOP), Timestamps
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        Timestamps: TSval 15432207, TSecr 3771086418
            Kind: Timestamp (8)
            Length: 10
            Timestamp value: 15432207
            Timestamp echo reply: 3771086418
    [SEQ/ACK analysis]
        [This is an ACK to the segment in frame: 4]
        [The RTT to ACK the segment was: 0.000044143 seconds]

0000  08 00 27 41 52 ce 08 00 27 2b 02 de 08 00 45 00   ..'AR...'+....E.
0010  00 34 9c aa 40 00 40 06 85 fe 0a 00 02 0f 0a 00   .4..@.@.........
0020  02 0d 00 50 89 4a ae 72 e6 a5 d7 19 3e a3 80 10   ...P.J.r....>...
0030  00 e3 18 42 00 00 01 01 08 0a 00 eb 7a 0f e0 c6   ...B........z...
0040  36 52                                             6R
No.     Time           Source                Destination           Protocol Length Info
      6 0.005039428    10.0.2.15             10.0.2.13             HTTP     344    HTTP/1.1 200 OK  (text/html)

Frame 6: 344 bytes on wire (2752 bits), 344 bytes captured (2752 bits) on interface 0
    Interface id: 0
    Encapsulation type: Ethernet (1)
    Arrival Time: Aug 20, 2023 23:43:07.740786511 EEST
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1692564187.740786511 seconds
    [Time delta from previous captured frame: 0.004183517 seconds]
    [Time delta from previous displayed frame: 0.004183517 seconds]
    [Time since reference or first frame: 0.005039428 seconds]
    Frame Number: 6
    Frame Length: 344 bytes (2752 bits)
    Capture Length: 344 bytes (2752 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: eth:ip:tcp:http:data-text-lines]
    [Number of per-protocol-data: 1]
    [Hypertext Transfer Protocol, key 0]
    [Coloring Rule Name: HTTP]
    [Coloring Rule String: http || tcp.port == 80]
Ethernet II, Src: CadmusCo_2b:02:de (08:00:27:2b:02:de), Dst: CadmusCo_41:52:ce (08:00:27:41:52:ce)
    Destination: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        Address: CadmusCo_41:52:ce (08:00:27:41:52:ce)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Source: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        Address: CadmusCo_2b:02:de (08:00:27:2b:02:de)
        .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
        .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
    Type: IP (0x0800)
Internet Protocol Version 4, Src: 10.0.2.15 (10.0.2.15), Dst: 10.0.2.13 (10.0.2.13)
    Version: 4
    Header length: 20 bytes
    Differentiated Services Field: 0x00 (DSCP 0x00: Default; ECN: 0x00: Not-ECT (Not ECN-Capable Transport))
        0000 00.. = Differentiated Services Codepoint: Default (0x00)
        .... ..00 = Explicit Congestion Notification: Not-ECT (Not ECN-Capable Transport) (0x00)
    Total Length: 330
    Identification: 0x9cab (40107)
    Flags: 0x02 (Don't Fragment)
        0... .... = Reserved bit: Not set
        .1.. .... = Don't fragment: Set
        ..0. .... = More fragments: Not set
    Fragment offset: 0
    Time to live: 64
    Protocol: TCP (6)
    Header checksum: 0x84e7 [validation disabled]
        [Good: False]
        [Bad: False]
    Source: 10.0.2.15 (10.0.2.15)
    Destination: 10.0.2.13 (10.0.2.13)
Transmission Control Protocol, Src Port: http (80), Dst Port: 35146 (35146), Seq: 1, Ack: 104, Len: 278
    Source port: http (80)
    Destination port: 35146 (35146)
    [Stream index: 0]
    Sequence number: 1    (relative sequence number)
    [Next sequence number: 279    (relative sequence number)]
    Acknowledgment number: 104    (relative ack number)
    Header length: 32 bytes
    Flags: 0x018 (PSH, ACK)
        000. .... .... = Reserved: Not set
        ...0 .... .... = Nonce: Not set
        .... 0... .... = Congestion Window Reduced (CWR): Not set
        .... .0.. .... = ECN-Echo: Not set
        .... ..0. .... = Urgent: Not set
        .... ...1 .... = Acknowledgment: Set
        .... .... 1... = Push: Set
        .... .... .0.. = Reset: Not set
        .... .... ..0. = Syn: Not set
        .... .... ...0 = Fin: Not set
    Window size value: 227
    [Calculated window size: 29056]
    [Window size scaling factor: 128]
    Checksum: 0x1958 [validation disabled]
        [Good Checksum: False]
        [Bad Checksum: False]
    Options: (12 bytes), No-Operation (NOP), No-Operation (NOP), Timestamps
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        No-Operation (NOP)
            Type: 1
                0... .... = Copy on fragmentation: No
                .00. .... = Class: Control (0)
                ...0 0001 = Number: No-Operation (NOP) (1)
        Timestamps: TSval 15432210, TSecr 3771086418
            Kind: Timestamp (8)
            Length: 10
            Timestamp value: 15432210
            Timestamp echo reply: 3771086418
    [SEQ/ACK analysis]
        [Bytes in flight: 278]
Hypertext Transfer Protocol
    HTTP/1.1 200 OK\r\n
        [Expert Info (Chat/Sequence): HTTP/1.1 200 OK\r\n]
            [Message: HTTP/1.1 200 OK\r\n]
            [Severity level: Chat]
            [Group: Sequence]
        Request Version: HTTP/1.1
        Status Code: 200
        Response Phrase: OK
    Date: Sun, 20 Aug 2023 20:43:07 GMT\r\n
    Server: Apache/2.4.6 (CentOS) PHP/7.4.33\r\n
    X-Powered-By: PHP/7.4.33\r\n
    Content-Length: 94\r\n
        [Content length: 94]
    Content-Type: text/html; charset=UTF-8\r\n
    \r\n
    [HTTP response 1/1]
    [Time since request: 0.004227660 seconds]
    [Request in frame: 4]
Line-based text data: text/html
    \n
    Hello test!<br /><br />CMS User Panel<br /><br />articles<br />categories<br />comments<br />

0000  08 00 27 41 52 ce 08 00 27 2b 02 de 08 00 45 00   ..'AR...'+....E.
0010  01 4a 9c ab 40 00 40 06 84 e7 0a 00 02 0f 0a 00   .J..@.@.........
0020  02 0d 00 50 89 4a ae 72 e6 a5 d7 19 3e a3 80 18   ...P.J.r....>...
0030  00 e3 19 58 00 00 01 01 08 0a 00 eb 7a 12 e0 c6   ...X........z...
0040  36 52 48 54 54 50 2f 31 2e 31 20 32 30 30 20 4f   6RHTTP/1.1 200 O
0050  4b 0d 0a 44 61 74 65 3a 20 53 75 6e 2c 20 32 30   K..Date: Sun, 20
0060  20 41 75 67 20 32 30 32 33 20 32 30 3a 34 33 3a    Aug 2023 20:43:
0070  30 37 20 47 4d 54 0d 0a 53 65 72 76 65 72 3a 20   07 GMT..Server:
0080  41 70 61 63 68 65 2f 32 2e 34 2e 36 20 28 43 65   Apache/2.4.6 (Ce
0090  6e 74 4f 53 29 20 50 48 50 2f 37 2e 34 2e 33 33   ntOS) PHP/7.4.33
00a0  0d 0a 58 2d 50 6f 77 65 72 65 64 2d 42 79 3a 20   ..X-Powered-By:
00b0  50 48 50 2f 37 2e 34 2e 33 33 0d 0a 43 6f 6e 74   PHP/7.4.33..Cont
00c0  65 6e 74 2d 4c 65 6e 67 74 68 3a 20 39 34 0d 0a   ent-Length: 94..
00d0  43 6f 6e 74 65 6e 74 2d 54 79 70 65 3a 20 74 65   Content-Type: te
00e0  78 74 2f 68 74 6d 6c 3b 20 63 68 61 72 73 65 74   xt/html; charset
00f0  3d 55 54 46 2d 38 0d 0a 0d 0a 0a 48 65 6c 6c 6f   =UTF-8.....Hello
0100  20 74 65 73 74 21 3c 62 72 20 2f 3e 3c 62 72 20    test!<br /><br
0110  2f 3e 43 4d 53 20 55 73 65 72 20 50 61 6e 65 6c   />CMS User Panel
0120  3c 62 72 20 2f 3e 3c 62 72 20 2f 3e 61 72 74 69   <br /><br />arti
0130  63 6c 65 73 3c 62 72 20 2f 3e 63 61 74 65 67 6f   cles<br />catego
0140  72 69 65 73 3c 62 72 20 2f 3e 63 6f 6d 6d 65 6e   ries<br />commen
0150  74 73 3c 62 72 20 2f 3e                           ts<br />
No.     Time           Source                Destination           Protocol Length Info
      7 0.005756862    10.0.2.13             10.0.2.15             TCP      66     35146 > http [ACK] Seq=104 Ack=279 Win=64128 Len=0 TSval=3771086423 TSecr=15432210
	  
    4. Perform network bandwidth testing using TCP, UDP, and SCTP* protocols using Iperf3. (https://en.wikipedia.org/wiki/Iperf). Public Iperf3 servers
    (https://iperf.cc/)	  

[ec2-user@amazonlinux ~]$ iperf3 -c 10.5.5.1 -B 10.5.5.26
Connecting to host 10.5.5.1, port 5201
[  4] local 10.5.5.26 port 37803 connected to 10.5.5.1 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec   145 MBytes  1.22 Gbits/sec  362    198 KBytes
[  4]   1.00-2.00   sec   152 MBytes  1.27 Gbits/sec  442    197 KBytes
[  4]   2.00-3.00   sec   159 MBytes  1.33 Gbits/sec  349    199 KBytes
[  4]   3.00-4.00   sec   153 MBytes  1.28 Gbits/sec  591    205 KBytes
[  4]   4.00-5.00   sec   155 MBytes  1.30 Gbits/sec  285    173 KBytes
[  4]   5.00-6.00   sec   152 MBytes  1.28 Gbits/sec  319    182 KBytes
[  4]   6.00-7.00   sec   150 MBytes  1.26 Gbits/sec  494    252 KBytes
[  4]   7.00-8.00   sec   145 MBytes  1.21 Gbits/sec  329    242 KBytes
[  4]   8.00-9.00   sec   135 MBytes  1.13 Gbits/sec  299    187 KBytes
[  4]   9.00-10.00  sec   149 MBytes  1.25 Gbits/sec  333    197 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  1.46 GBytes  1.25 Gbits/sec  3803             sender
[  4]   0.00-10.00  sec  1.46 GBytes  1.25 Gbits/sec                  receiver

iperf Done.

ubuntu@ip-172-31-42-130:~$ iperf3 -s -B 10.5.5.1 -i 1 -p 5201
-----------------------------------------------------------
Server listening on 5201
-----------------------------------------------------------
Accepted connection from 10.5.5.26, port 45563
[  5] local 10.5.5.1 port 5201 connected to 10.5.5.26 port 36451
[ ID] Interval           Transfer     Bitrate
[  5]   0.00-1.00   sec   150 MBytes  1.26 Gbits/sec
[  5]   1.00-2.00   sec   157 MBytes  1.31 Gbits/sec
[  5]   2.00-3.00   sec   152 MBytes  1.28 Gbits/sec
[  5]   3.00-4.00   sec   159 MBytes  1.33 Gbits/sec
[  5]   4.00-5.00   sec   159 MBytes  1.34 Gbits/sec
[  5]   5.00-6.00   sec   147 MBytes  1.23 Gbits/sec
[  5]   6.00-7.00   sec   126 MBytes  1.06 Gbits/sec
[  5]   7.00-8.00   sec   154 MBytes  1.29 Gbits/sec
[  5]   8.00-9.00   sec   149 MBytes  1.25 Gbits/sec
[  5]   9.00-10.00  sec   149 MBytes  1.25 Gbits/sec
[  5]  10.00-10.04  sec  5.30 MBytes  1.12 Gbits/sec
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate
[  5]   0.00-10.04  sec  1.47 GBytes  1.26 Gbits/sec                  receiver

root@ip-172-31-42-130:~# tshark -r capture.pcap
Running as user "root" and group "root". This could be dangerous.
    1 0.000000000 PcsCompu_b1:8b:27 → Broadcast    ARP 60 Who has 10.5.5.1? Tell 10.5.5.26
    2 0.000032083 PcsCompu_05:6d:d5 → PcsCompu_b1:8b:27 ARP 42 10.5.5.1 is at 08:00:27:05:6d:d5
    3 0.000417363    10.5.5.26 → 10.5.5.1     TCP 74 46839 → 5201 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM TSval=1478791482 TSecr=0 WS=128
    4 0.000503501     10.5.5.1 → 10.5.5.26    TCP 74 5201 → 46839 [SYN, ACK] Seq=0 Ack=1 Win=65160 Len=0 MSS=1460 SACK_PERM TSval=1103905354 TSecr=1478791482 WS=128
    5 0.000906084    10.5.5.26 → 10.5.5.1     TCP 66 46839 → 5201 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=1478791483 TSecr=1103905354
    6 0.001160403    10.5.5.26 → 10.5.5.1     TCP 103 46839 → 5201 [PSH, ACK] Seq=1 Ack=1 Win=29312 Len=37 TSval=1478791483 TSecr=1103905354
    7 0.001191730     10.5.5.1 → 10.5.5.26    TCP 66 5201 → 46839 [ACK] Seq=1 Ack=38 Win=65152 Len=0 TSval=1103905354 TSecr=1478791483
    8 0.001306268     10.5.5.1 → 10.5.5.26    TCP 67 5201 → 46839 [PSH, ACK] Seq=1 Ack=38 Win=65152 Len=1 TSval=1103905354 TSecr=1478791483
    9 0.001785067    10.5.5.26 → 10.5.5.1     TCP 66 46839 → 5201 [ACK] Seq=38 Ack=2 Win=29312 Len=0 TSval=1478791484 TSecr=1103905354
   10 0.002046425    10.5.5.26 → 10.5.5.1     TCP 70 46839 → 5201 [PSH, ACK] Seq=38 Ack=2 Win=29312 Len=4 TSval=1478791484 TSecr=1103905354
   11 0.045499019     10.5.5.1 → 10.5.5.26    TCP 66 5201 → 46839 [ACK] Seq=2 Ack=42 Win=65152 Len=0 TSval=1103905399 TSecr=1478791484
   12 0.046083923    10.5.5.26 → 10.5.5.1     TCP 148 46839 → 5201 [PSH, ACK] Seq=42 Ack=2 Win=29312 Len=82 TSval=1478791528 TSecr=1103905399
   13 0.046111104     10.5.5.1 → 10.5.5.26    TCP 66 5201 → 46839 [ACK] Seq=2 Ack=124 Win=65152 Len=0 TSval=1103905399 TSecr=1478791528
   14 0.046334243     10.5.5.1 → 10.5.5.26    TCP 67 5201 → 46839 [PSH, ACK] Seq=2 Ack=124 Win=65152 Len=1 TSval=1103905400 TSecr=1478791528
   15 0.046906167    10.5.5.26 → 10.5.5.1     TCP 74 37803 → 5201 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM TSval=1478791529 TSecr=0 WS=128
   16 0.046965664     10.5.5.1 → 10.5.5.26    TCP 74 5201 → 37803 [SYN, ACK] Seq=0 Ack=1 Win=65160 Len=0 MSS=1460 SACK_PERM TSval=1103905400 TSecr=1478791529 WS=128
   17 0.047405788    10.5.5.26 → 10.5.5.1     TCP 66 37803 → 5201 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=1478791530 TSecr=1103905400
   18 0.047617100    10.5.5.26 → 10.5.5.1     TCP 103 37803 → 5201 [PSH, ACK] Seq=1 Ack=1 Win=29312 Len=37 TSval=1478791530 TSecr=1103905400
   19 0.047642815     10.5.5.1 → 10.5.5.26    TCP 66 5201 → 37803 [ACK] Seq=1 Ack=38 Win=65152 Len=0 TSval=1103905401 TSecr=1478791530
   20 0.089547516    10.5.5.26 → 10.5.5.1     TCP 66 46839 → 5201 [ACK] Seq=124 Ack=3 Win=29312 Len=0 TSval=1478791572 TSecr=1103905400

[ec2-user@amazonlinux ~]$ iperf3 -c 10.5.5.1 -B 10.5.5.26 -u
Connecting to host 10.5.5.1, port 5201
[  4] local 10.5.5.26 port 53973 connected to 10.5.5.1 port 5201
[ ID] Interval           Transfer     Bandwidth       Total Datagrams
[  4]   0.00-1.00   sec   116 KBytes   949 Kbits/sec  82
[  4]   1.00-2.00   sec   129 KBytes  1.05 Mbits/sec  91
[  4]   2.00-3.00   sec   127 KBytes  1.04 Mbits/sec  90
[  4]   3.00-4.00   sec   129 KBytes  1.05 Mbits/sec  91
[  4]   4.00-5.00   sec   127 KBytes  1.04 Mbits/sec  90
[  4]   5.00-6.00   sec   129 KBytes  1.05 Mbits/sec  91
[  4]   6.00-7.00   sec   127 KBytes  1.04 Mbits/sec  90
[  4]   7.00-8.00   sec   129 KBytes  1.05 Mbits/sec  91
[  4]   8.00-9.00   sec   127 KBytes  1.04 Mbits/sec  90
[  4]   9.00-10.00  sec   129 KBytes  1.05 Mbits/sec  91
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-10.00  sec  1.24 MBytes  1.04 Mbits/sec  0.114 ms  0/897 (0%)
[  4] Sent 897 datagrams

iperf Done.

 ubuntu@ip-172-31-42-130:~$ iperf3 -s -B 10.5.5.1 -i 1 -p 5201
-----------------------------------------------------------
Server listening on 5201
-----------------------------------------------------------
Accepted connection from 10.5.5.26, port 40999
[  5] local 10.5.5.1 port 5201 connected to 10.5.5.26 port 53973
[ ID] Interval           Transfer     Bitrate         Jitter    Lost/Total Datagrams
[  5]   0.00-1.00   sec   116 KBytes   949 Kbits/sec  0.159 ms  0/82 (0%)
[  5]   1.00-2.00   sec   129 KBytes  1.05 Mbits/sec  0.104 ms  0/91 (0%)
[  5]   2.00-3.00   sec   127 KBytes  1.04 Mbits/sec  0.135 ms  0/90 (0%)
[  5]   3.00-4.00   sec   129 KBytes  1.05 Mbits/sec  0.107 ms  0/91 (0%)
[  5]   4.00-5.00   sec   127 KBytes  1.04 Mbits/sec  0.114 ms  0/90 (0%)
[  5]   5.00-6.00   sec   129 KBytes  1.05 Mbits/sec  0.136 ms  0/91 (0%)
[  5]   6.00-7.00   sec   127 KBytes  1.04 Mbits/sec  0.113 ms  0/90 (0%)
[  5]   7.00-8.00   sec   129 KBytes  1.05 Mbits/sec  0.133 ms  0/91 (0%)
[  5]   8.00-9.00   sec   127 KBytes  1.04 Mbits/sec  0.108 ms  0/90 (0%)
[  5]   9.00-10.00  sec   129 KBytes  1.05 Mbits/sec  0.114 ms  0/91 (0%)
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Jitter    Lost/Total Datagrams
[  5]   0.00-10.04  sec  1.24 MBytes  1.03 Mbits/sec  0.114 ms  0/897 (0%)  receiver

root@ip-172-31-42-130:~# tshark -r capture.pcap
Running as user "root" and group "root". This could be dangerous.
    1 0.000000000 PcsCompu_b1:8b:27 → Broadcast    ARP 60 Who has 10.5.5.1? Tell 10.5.5.26
    2 0.000033839 PcsCompu_05:6d:d5 → PcsCompu_b1:8b:27 ARP 42 10.5.5.1 is at 08:00:27:05:6d:d5
    3 0.000422044    10.5.5.26 → 10.5.5.1     DHCP 342 DHCP Request  - Transaction ID 0xa32b2f0b
    4 0.222352652     10.5.5.1 → 10.5.5.26    DHCP 347 DHCP ACK      - Transaction ID 0xa32b2f0b
    5 0.282206158 PcsCompu_b1:8b:27 → Broadcast    ARP 60 Who has 10.5.5.26? Tell 0.0.0.0
    6 1.282967912 PcsCompu_b1:8b:27 → Broadcast    ARP 60 Who has 10.5.5.26? Tell 0.0.0.0
    7 5.364845002 PcsCompu_05:6d:d5 → PcsCompu_b1:8b:27 ARP 42 Who has 10.5.5.26? Tell 10.5.5.1
    8 5.365452260 PcsCompu_b1:8b:27 → PcsCompu_05:6d:d5 ARP 60 10.5.5.26 is at 08:00:27:b1:8b:27
    9 37.229699989    10.5.5.26 → 10.5.5.1     TCP 74 40999 → 5201 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM TSval=1479465371 TSecr=0 WS=128
   10 37.229797245     10.5.5.1 → 10.5.5.26    TCP 74 5201 → 40999 [SYN, ACK] Seq=0 Ack=1 Win=65160 Len=0 MSS=1460 SACK_PERM TSval=1104579247 TSecr=1479465371 WS=128
   11 37.230156298    10.5.5.26 → 10.5.5.1     TCP 66 40999 → 5201 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=1479465372 TSecr=1104579247
   12 37.230378458    10.5.5.26 → 10.5.5.1     TCP 103 40999 → 5201 [PSH, ACK] Seq=1 Ack=1 Win=29312 Len=37 TSval=1479465372 TSecr=1104579247
   13 37.230403896     10.5.5.1 → 10.5.5.26    TCP 66 5201 → 40999 [ACK] Seq=1 Ack=38 Win=65152 Len=0 TSval=1104579248 TSecr=1479465372
   14 37.230608988     10.5.5.1 → 10.5.5.26    TCP 67 5201 → 40999 [PSH, ACK] Seq=1 Ack=38 Win=65152 Len=1 TSval=1104579248 TSecr=1479465372
   15 37.231028647    10.5.5.26 → 10.5.5.1     TCP 66 40999 → 5201 [ACK] Seq=38 Ack=2 Win=29312 Len=0 TSval=1479465373 TSecr=1104579248
   16 37.231216375    10.5.5.26 → 10.5.5.1     TCP 70 40999 → 5201 [PSH, ACK] Seq=38 Ack=2 Win=29312 Len=4 TSval=1479465373 TSecr=1104579248
   17 37.272399299     10.5.5.1 → 10.5.5.26    TCP 66 5201 → 40999 [ACK] Seq=2 Ack=42 Win=65152 Len=0 TSval=1104579290 TSecr=1479465373
   18 37.272984399    10.5.5.26 → 10.5.5.1     TCP 166 40999 → 5201 [PSH, ACK] Seq=42 Ack=2 Win=29312 Len=100 TSval=1479465415 TSecr=1104579290
   19 37.273011770     10.5.5.1 → 10.5.5.26    TCP 66 5201 → 40999 [ACK] Seq=2 Ack=142 Win=65152 Len=0 TSval=1104579290 TSecr=1479465415
   20 37.273306685     10.5.5.1 → 10.5.5.26    TCP 67 5201 → 40999 [PSH, ACK] Seq=2 Ack=142 Win=65152 Len=1 TSval=1104579291 TSecr=1479465415

   6.1.c. Host diagnostics
   1. Monitor network activity of the local system (commands: netstat, ss, iptraf, nc)
   a. Detection of active connections

ubuntu@ip-172-31-42-130:~$ netstat -an | grep ESTABLISHED
tcp        0     52 192.168.0.77:22         192.168.0.80:58466      ESTABLISHED
ubuntu@ip-172-31-42-130:~$ ss -t -a | grep ESTAB
ESTAB  0      0       192.168.0.77:ssh    192.168.0.80:58466

The output of the netstat and ss commands indicate that there is an active connection between the local system (192.168.0.77) and a remote system (192.168.0.80) on port 22, which is the default port for SSH (Secure Shell) connections. The connection is in the ESTABLISHED state, which means that data is being exchanged between the two systems.

   b. analyze open ports (UDP, TCP). Give their classification

ubuntu@ip-172-31-42-130:~$ ss -tunlp
Netid  State    Recv-Q   Send-Q        Local Address:Port     Peer Address:Port  Process
udp    UNCONN   0        0             127.0.0.53%lo:53            0.0.0.0:*
udp    UNCONN   0        0                   0.0.0.0:67            0.0.0.0:*
udp    UNCONN   0        0          10.0.2.13%enp0s3:68            0.0.0.0:*
tcp    LISTEN   0        4096          127.0.0.53%lo:53            0.0.0.0:*
tcp    LISTEN   0        128                 0.0.0.0:22            0.0.0.0:*
tcp    LISTEN   0        128                    [::]:22               [::]:*
ubuntu@ip-172-31-42-130:~$ sudo lsof -iTCP -sTCP:LISTEN
COMMAND   PID            USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
systemd-r 642 systemd-resolve   14u  IPv4  20057      0t0  TCP localhost:domain (LISTEN)
sshd      745            root    3u  IPv4  20778      0t0  TCP *:ssh (LISTEN)
sshd      745            root    4u  IPv6  20791      0t0  TCP *:ssh (LISTEN)
ubuntu@ip-172-31-42-130:~$ sudo netstat -tunlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      745/sshd: /usr/sbin
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      642/systemd-resolve
tcp6       0      0 :::22                   :::*                    LISTEN      745/sshd: /usr/sbin
udp        0      0 127.0.0.53:53           0.0.0.0:*                           642/systemd-resolve
udp        0      0 0.0.0.0:67              0.0.0.0:*                           662/dhcpd 
udp        0      0 10.0.2.13:68            0.0.0.0:*                           640/systemd-network


    TCP port 22: This is a well-known port used for SSH (Secure Shell) connections.
    UDP port 53: This is a well-known port used for DNS (Domain Name System) queries.
    UDP port 67: This is a well-known port used for DHCP (Dynamic Host Configuration Protocol) server.
    UDP port 68: This is a well-known port used for DHCP (Dynamic Host Configuration Protocol) client.

   
   c. explain the state of network connections (see above)
   d. determine the main, running network services (processes). Which of them work in the mode server

ubuntu@ip-172-31-42-130:~$ ps -p 745 -o comm=
sshd
ubuntu@ip-172-31-42-130:~$ ps -p 642 -o comm=
systemd-resolve
ubuntu@ip-172-31-42-130:~$ ps -p 662 -o comm=
dhcpd
ubuntu@ip-172-31-42-130:~$ ps -p 640 -o comm=
systemd-network
   
   e. explain what state the connection is in
   
   

    TCP port 22: The connection is in the LISTEN state, which means that the server is waiting for incoming connections from clients.

    TCP port 53: The connection is in the LISTEN state, which means that the DNS resolver is waiting for incoming DNS queries.

    UDP port 67: The connection is in the LISTEN state, which means that the DHCP server is waiting for incoming DHCP requests.

    UDP port 68: The connection is in the ESTABLISHED state, which means that the DHCP client has an active connection with the DHCP server.

   f. Use the ping command to build a packet route through routers to recipient. Write a script (python, java*, ruby*).

ubuntu@ip-172-31-42-130:~$ ip r s
default via 192.168.0.1 dev enp0s8 proto static
default via 10.0.2.1 dev enp0s3 proto dhcp src 10.0.2.13 metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.13 metric 100
10.0.2.1 dev enp0s3 proto dhcp scope link src 10.0.2.13 metric 100
10.5.5.0/27 dev enp0s9 proto kernel scope link src 10.5.5.1
192.168.0.0/24 dev enp0s8 proto kernel scope link src 192.168.0.77
192.168.0.1 via 10.0.2.1 dev enp0s3 proto dhcp src 10.0.2.13 metric 100

ubuntu@ip-172-31-42-130:~$ ping -R -c 2 10.5.5.26
PING 10.5.5.26 (10.5.5.26) 56(124) bytes of data.
64 bytes from 10.5.5.26: icmp_seq=1 ttl=255 time=0.605 ms
RR:     10.5.5.1
        10.5.5.26
        10.5.5.26
        10.5.5.1

64 bytes from 10.5.5.26: icmp_seq=2 ttl=255 time=0.743 ms       (same route)

--- 10.5.5.26 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.605/0.674/0.743/0.069 ms

import argparse
import subprocess

class PacketRoute:
    def __init__(self, recipient_ip, ping_count=2):
        self.recipient_ip = recipient_ip
        self.ping_count = ping_count
        self.route = []

    def build(self):
        ping_command = ["ping", "-R", "-c", str(self.ping_count), self.recipient_ip]
        ping_output = subprocess.check_output(ping_command).decode("utf-8")

        for line in ping_output.splitlines():
            if "RR:" in line:
                self.route.extend(line.split()[1:])

    def print_route(self):
        print("Packet route to", self.recipient_ip, ":", self.route)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Build a packet route through routers to a recipient.")
    parser.add_argument("recipient_ip", help="IP address of the recipient")
    parser.add_argument("-c", "--count", type=int, default=2, help="number of packets to send (default: 2)")
    args = parser.parse_args()

    route = PacketRoute(args.recipient_ip, ping_count=args.count)
    route.build()
    route.print_route()
	
ubuntu@ip-172-31-42-130:~$ python3 buildroute.py 10.5.5.26 -c 3
Packet route to 10.5.5.26 : ['10.5.5.1']
   
   2. Check open ports using TCP/UDP protocols (netstat, ss, iptraf, nc, lsof):
   a. on the local host;

ubuntu@ip-172-31-42-130:~$ ping -I enp0s9 -c 2 10.5.5.26
PING 10.5.5.26 (10.5.5.26) from 10.5.5.1 enp0s9: 56(84) bytes of data.
64 bytes from 10.5.5.26: icmp_seq=1 ttl=255 time=0.636 ms
64 bytes from 10.5.5.26: icmp_seq=2 ttl=255 time=1.07 ms

--- 10.5.5.26 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.636/0.852/1.068/0.216 ms

[ec2-user@amazonlinux ~]$ sudo iptraf-ng
 iptraf-ng 1.1.4
┌ TCP Connections (Source Host:Port) ─────────────── Packets ───── Bytes  Flag Iface ────┐│                                                                                                                                                                              ││                                                                                        │└ TCP:      0 entries ────────────────────────────────────────────────────────── Active ─┘┌────────────────────────────────────────────────────────────────────────────────────────┐│ ICMP echo req (84 bytes) from 10.5.5.1 to 10.5.5.26 on eth2                            ││ ICMP echo rply (84 bytes) from 10.5.5.26 to 10.5.5.1 on eth2                           ││ UDP (328 bytes) from 10.5.5.26:68 to 10.5.5.1:67 on eth2                               ││ UDP (328 bytes) from 10.5.5.26:68 to 10.5.5.1:67 on eth2                               ││ UDP (333 bytes) from 10.5.5.1:67 to 10.5.5.26:68 on eth2                               ││ UDP (333 bytes) from 10.5.5.1:67 to 10.5.5.26:68 on eth2                               ││ UDP (333 bytes) from 10.5.5.1:67 to 10.5.5.26:68 on eth2                               │└ Bottom ────── Elapsed time:   0:01 ────────────────────────────────────────────────────┘ Packets captured:                            16  │ No TCP entries  

[ec2-user@amazonlinux ~]$ netstat -tuln
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
tcp6       0      0 :::111                  :::*                    LISTEN
tcp6       0      0 :::22                   :::*                    LISTEN
udp        0      0 0.0.0.0:803             0.0.0.0:*
udp        0      0 0.0.0.0:68              0.0.0.0:*
udp        0      0 0.0.0.0:68              0.0.0.0:*
udp        0      0 0.0.0.0:68              0.0.0.0:*
udp        0      0 0.0.0.0:111             0.0.0.0:*
udp        0      0 127.0.0.1:323           0.0.0.0:*
udp6       0      0 :::803                  :::*
udp6       0      0 :::111                  :::*
udp6       0      0 ::1:323                 :::*

[ec2-user@amazonlinux ~]$ ss -tuln
Netid   State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port   Process
udp     UNCONN   0        0                0.0.0.0:803           0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:68            0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:68            0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:68            0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:111           0.0.0.0:*
udp     UNCONN   0        0              127.0.0.1:323           0.0.0.0:*
udp     UNCONN   0        0                   [::]:803              [::]:*
udp     UNCONN   0        0                   [::]:111              [::]:*
udp     UNCONN   0        0                  [::1]:323              [::]:*
tcp     LISTEN   0        128              0.0.0.0:111           0.0.0.0:*
tcp     LISTEN   0        128              0.0.0.0:22            0.0.0.0:*
tcp     LISTEN   0        100            127.0.0.1:25            0.0.0.0:*
tcp     LISTEN   0        128                 [::]:111              [::]:*
tcp     LISTEN   0        128                 [::]:22               [::]:*

[ec2-user@amazonlinux ~]$ sudo nc -zv localhost 1-65535
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connection refused.

[ec2-user@amazonlinux ~]$ sudo lsof -i -P -n | grep LISTEN
rpcbind  1901      rpc    8u  IPv4  16411      0t0  TCP *:111 (LISTEN)
rpcbind  1901      rpc   11u  IPv6  16414      0t0  TCP *:111 (LISTEN)
master   2592     root   13u  IPv4  18876      0t0  TCP 127.0.0.1:25 (LISTEN)
sshd     2629     root    3u  IPv4  19466      0t0  TCP *:22 (LISTEN)
sshd     2629     root    4u  IPv6  19468      0t0  TCP *:22 (LISTEN)


   
   b. on a remote host;

[ec2-user@amazonlinux ~]$ netstat -tuln | ssh -i .ssh/j2.pem ubuntu@10.5.5.1 'cat -'
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
tcp6       0      0 :::111                  :::*                    LISTEN
tcp6       0      0 :::22                   :::*                    LISTEN
udp        0      0 0.0.0.0:803             0.0.0.0:*
udp        0      0 0.0.0.0:68              0.0.0.0:*
udp        0      0 0.0.0.0:68              0.0.0.0:*
udp        0      0 0.0.0.0:68              0.0.0.0:*
udp        0      0 0.0.0.0:111             0.0.0.0:*
udp        0      0 127.0.0.1:323           0.0.0.0:*
udp6       0      0 :::803                  :::*
udp6       0      0 :::111                  :::*
udp6       0      0 ::1:323                 :::*

[ec2-user@amazonlinux ~]$ ss -tuln | ssh -i .ssh/j2.pem ubuntu@10.5.5.1 'cat -'
Netid State  Recv-Q Send-Q Local Address:Port Peer Address:PortProcess
udp   UNCONN 0      0            0.0.0.0:803       0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:68        0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:68        0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:68        0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:111       0.0.0.0:*
udp   UNCONN 0      0          127.0.0.1:323       0.0.0.0:*
udp   UNCONN 0      0               [::]:803          [::]:*
udp   UNCONN 0      0               [::]:111          [::]:*
udp   UNCONN 0      0              [::1]:323          [::]:*
tcp   LISTEN 0      128          0.0.0.0:111       0.0.0.0:*
tcp   LISTEN 0      128          0.0.0.0:22        0.0.0.0:*
tcp   LISTEN 0      100        127.0.0.1:25        0.0.0.0:*
tcp   LISTEN 0      128             [::]:111          [::]:*
tcp   LISTEN 0      128             [::]:22           [::]:*

[ec2-user@amazonlinux ~]$ nc -zv 10.5.5.1 1-65535
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connection refused.
[ec2-user@amazonlinux ~]$ nmap -p 1-65535 10.5.5.1

Starting Nmap 6.40 ( http://nmap.org ) at 2023-08-21 19:44 UTC
Nmap scan report for 10.5.5.1
Host is up (0.0022s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 11.63 seconds

[ec2-user@amazonlinux ~]$ nmap -p 80,443 10.5.5.1

Starting Nmap 6.40 ( http://nmap.org ) at 2023-08-21 19:53 UTC
Nmap scan report for 10.5.5.1
Host is up (0.00077s latency).
PORT    STATE  SERVICE
80/tcp  closed http
443/tcp closed https
   
   c. explain the principle of verification, what it is based on (a, b)

By checking for open ports on local and remote hosts, you can identify potential security risks and take steps to mitigate them. For example, if you find that a port is open on a system that should not be open, you can close the port or restrict access to it to prevent unauthorized access. Similarly, if you find that a port is closed on a system that should be open, you can troubleshoot the issue and ensure that the necessary services are running and accessible.

Checking for open ports can also help you ensure that your network is functioning properly. For example, if you are experiencing network connectivity issues, checking for open ports can help you identify potential issues with firewalls, routers, or other network devices that may be blocking traffic. By identifying and resolving these issues, you can ensure that your network is functioning properly and that data is being transmitted and received correctly.

   d. Offer utils for checking open ports (for Linux OS, containers).

    For Linux OS:

    netstat: This command displays a list of all open TCP and UDP ports on the local host. For example, you can run the following command to display a list of all open TCP and UDP ports on the local host:

    netstat -tuln

    ss: This command is similar to netstat and displays a list of all open TCP and UDP ports on the local host. For example, you can run the following command to display a list of all open TCP and UDP ports on the local host using ss:

    ss -tuln

    lsof: This command displays a list of all open files, including network sockets, on the local host. For example, you can run the following command to display a list of all open network sockets on the local host using lsof:

    sudo lsof -i -P -n | grep LISTEN

    For containers:

    docker ps: This command displays a list of all running containers on the local host. For example, you can run the following command to display a list of all running containers on the local host:

    docker ps

    docker port: This command displays the public-facing port of a container. For example, you can run the following command to display the public-facing port of a container with ID CONTAINER_ID:

    docker port CONTAINER_ID

    docker inspect: This command displays detailed information about a container, including its network settings. For example, you can run the following command to display detailed information about a container with ID CONTAINER_ID:

    docker inspect CONTAINER_ID

These utilities can help you check for open ports on Linux OS and containers, and identify potential security risks or network connectivity issues.


   e. * Implement a software way of checking ports. Can be used only (python, bash*, java**, ruby**).

#!/bin/bash

# Define functions for each command
function check_netstat {
    echo "Checking open ports on $1 using netstat..."
    netstat -tuln
}

function check_ss {
    echo "Checking open ports on $1 using ss..."
    ss -tuln
}

function check_nmap {
    echo "Checking open ports on $1 using nmap..."
    nmap -p 1-65535 $1
}

# Check the number of arguments
if [ $# -ne 2 ]; then
    echo "Usage: $0 <host> <command>"
    exit 1
fi

# Call the appropriate function based on the second argument
case $2 in
    netstat)
        check_netstat $1
        ;;
    ss)
        check_ss $1
        ;;
    nmap)
        check_nmap $1
        ;;
    *)
        echo "Invalid command: $2"
        exit 1
        ;;
esac   

[ec2-user@amazonlinux ~]$ ./script 10.5.5.26 ss
Checking open ports on 10.5.5.26 using ss...
Netid   State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port   Process
udp     UNCONN   0        0                0.0.0.0:803           0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:68            0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:68            0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:68            0.0.0.0:*
udp     UNCONN   0        0                0.0.0.0:111           0.0.0.0:*
udp     UNCONN   0        0              127.0.0.1:323           0.0.0.0:*
udp     UNCONN   0        0                   [::]:803              [::]:*
udp     UNCONN   0        0                   [::]:111              [::]:*
udp     UNCONN   0        0                  [::1]:323              [::]:*
tcp     LISTEN   0        128              0.0.0.0:111           0.0.0.0:*
tcp     LISTEN   0        128              0.0.0.0:22            0.0.0.0:*
tcp     LISTEN   0        100            127.0.0.1:25            0.0.0.0:*
tcp     LISTEN   0        128                 [::]:111              [::]:*
tcp     LISTEN   0        128                 [::]:22               [::]:*
[ec2-user@amazonlinux ~]$ ./script 10.5.5.1 ss | ssh -i .ssh/j2.pem ubuntu@10.5.5.1 'cat -'
Checking open ports on 10.5.5.1 using ss...
Netid State  Recv-Q Send-Q Local Address:Port Peer Address:PortProcess
udp   UNCONN 0      0            0.0.0.0:803       0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:68        0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:68        0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:68        0.0.0.0:*
udp   UNCONN 0      0            0.0.0.0:111       0.0.0.0:*
udp   UNCONN 0      0          127.0.0.1:323       0.0.0.0:*
udp   UNCONN 0      0               [::]:803          [::]:*
udp   UNCONN 0      0               [::]:111          [::]:*
udp   UNCONN 0      0              [::1]:323          [::]:*
tcp   LISTEN 0      128          0.0.0.0:111       0.0.0.0:*
tcp   LISTEN 0      128          0.0.0.0:22        0.0.0.0:*
tcp   LISTEN 0      100        127.0.0.1:25        0.0.0.0:*
tcp   LISTEN 0      128             [::]:111          [::]:*
tcp   LISTEN 0      128             [::]:22           [::]:*
</pre>
